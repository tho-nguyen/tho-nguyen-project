<?php
Route::group(['middleware' => ['auth', 'locale'], 'prefix' => 'admin'], function () {
    Route::resource('news', 'Admin\NewsController');
	Route::resource('categories', 'Admin\CategoryController');
	Route::resource('contacts', 'Admin\ContactController');
	// Route::put('ajaxStatus/updateModalStatus', 'AjaxController@updateModalStatus');
	Route::resource('sliders', 'Admin\SliderController');
	Route::resource('linhvuc', 'Admin\LinhvucController');
	Route::resource('baiviet', 'Admin\BaivietController');
	Route::resource('du-an', 'Admin\DuanController');
	Route::resource('batdongsan', 'Admin\BatDongSanController');
	
	Route::resource('vatlieu', 'Admin\VatlieuController');
	Route::resource('sanpham', 'Admin\SanPhamVatLieuController');
	Route::resource('gioithieu', 'Admin\GioithieuController');

	Route::resource('yeucau', 'Admin\YeuCauController');

});
Route::group(['middleware' => 'auth'], function () {

	Route::get('/', 'HomeController@index')->name('admin');
	Route::get('/admin', 'HomeController@index')->name('admin');

	Route::resource('admin/roles', 'Admin\RolesController');
	Route::resource('admin/permissions', 'Admin\PermissionsController');

	Route::resource('admin/users', 'Admin\UsersController');

	Route::get('profile', 'Admin\UsersController@getProfile');
	Route::post('profile', 'Admin\UsersController@postProfile');


	Route::get('ajax/{action}', 'AjaxController@index');


	Route::get('admin/settings', 'SettingController@index');
	Route::patch('admin/settings', 'SettingController@update');

	route::get('/login/account', 'UserController@getLogin')->name('get.login');

	route::post('/logout/account', 'UserController@getlogout');
	route::post('/login/account', 'UserController@postLogin');


	Route::get('/sentcontact/account', 'UserController@formcontact');
	Route::post('/sentcontact/account', 'UserController@postcontact');
});
