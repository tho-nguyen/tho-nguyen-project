<?php

namespace Modules\Theme\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Duan;

class DuAnController extends Controller
{
    public function index(){
        $category = "du an";
        $projects = Duan::where('active',1)->orderBy('created_at','DESC')->get();
        return view('front-end.layouts.du-an.bai-viet',compact('projects'));
    }

    public function show($slug){
        $projectDetail = Duan::where('slug',$slug)->first();
        $recentProject = Duan::where('active',1)->where('id','<>',$projectDetail->id)->orderBy('created_at','DESC')->take(6)->get();
        return view('front-end.layouts.du-an.bai-viet-chi-tiet',compact('projectDetail','recentProject'));
    }

}
