<?php

namespace Modules\Theme\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BatDongSan;

class BatDongSanController extends Controller
{
    
    public function index(){
        $batdongsan = BatDongSan::where('active',1)->orderBy('created_at','DESC')->get();
        return view('front-end.layouts.bat-dong-san.bai-viet',compact('batdongsan'));
    }

    public function show($slug){
        $bdsDetail = BatDongSan::where('slug',$slug)->first();
        $recentBds = BatDongSan::where('active',1)->where('id','<>',$bdsDetail->id)->orderBy('created_at','DESC')->take(6)->get();
        return view('front-end.layouts.bat-dong-san.bai-viet-chi-tiet',compact('bdsDetail','recentBds'));
    }
}
