<?php

namespace Modules\Theme\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\VatLieu;
use App\SanPhamVatLieu;

class VatLieuController extends Controller
{
    public function index($slug){
        $category = VatLieu::where('slug',$slug)->first();
        $products = SanPhamVatLieu::where('vatlieu_id',$category->id)->where('active',1)->orderBy('created_at','DESC')->get();
        return view('front-end.layouts.vat-lieu.vat-lieu',compact('category','products'));
    }
}
