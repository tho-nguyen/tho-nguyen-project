<?php

namespace Modules\Theme\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\News;

class NewsController extends Controller
{

    public function index($slug){
        $category = Category::where('slug', $slug)->first();
        $post     = News::where('category_id',$category->id)->where('active',1)->orderBy('created_at','DESC')->get();
        return view('front-end.layouts.tin-tuc.bai-viet',compact('post','category'));
    }

    public function show($slug){

        $postDetail = News::where('slug',$slug)->first();
        // GET POST
        $cate   = Category::where('id', $postDetail->category_id)->first();
        // GET CATEGORY
        $countPostCate = Category::withCount('news')->get();
        // dd($countPostCate)
        // COUNT POST BY CATEGORY
        $postRecent = News::where('active',1)->where('id','<>',$postDetail->id)->orderBy('created_at','DESC')->take(6)->get();
        // Get recent post
        return view('front-end.layouts.tin-tuc.bai-viet-chi-tiet',compact(['postDetail','cate','countPostCate','postRecent']));
    }

}
