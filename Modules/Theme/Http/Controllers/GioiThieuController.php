<?php

namespace Modules\Theme\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GioiThieu;
class GioiThieuController extends Controller
{
    public function index(){
        $introduce = GioiThieu::first();
        return view('front-end.layouts.gioi-thieu',compact('introduce'));
    }
}
