<?php


namespace Modules\Theme\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Slider;
use App\Danhmuclinhvuc;
use App\SanPhamVatLieu;
use App\Duan;
use App\BatDongSan;
use App\News;
use App\Category;
use App\Contact;
use Illuminate\Support\Str;

class HomeController extends Controller
{

    public function index()
    {
        $sliders = Slider::All();
        $categoriesLinhVuc = Danhmuclinhvuc::All();
        // GET DANH MUC LINH VUC 
        $productsVatLieu = SanPhamVatLieu::orderBy('id',"DESC")->take(8)->where("active",1)->get();
        // GET SAN PHAM VAT LIEU
        $Duan = Duan::orderBy('created_at','DESC')->take(6)->where("active",1)->get();
        // GET DU AN
        $BatDongSan = BatDongSan::orderBy('created_at','DESC')->take(6)->where("active",1)->get();
        // GET BAT DONG SAN
        $News = News::orderBy('created_at','DESC')->take(4)->where("is_focus",1)->get();
        // GET TIN NOI BAT
        $categoriesNews = Category::All();
        // GET DANH MUC TIN TUC
        return view('front-end.layouts.home',compact(['sliders','categoriesLinhVuc','productsVatLieu','Duan','BatDongSan','News','categoriesNews']));
    }

    public function store(Request $request){
        $dataInsert = $request->all();
        Contact::create($dataInsert);
        toastr()->success(__('Bài viết đã được lưu'));
        return redirect()->back();
    }
}
