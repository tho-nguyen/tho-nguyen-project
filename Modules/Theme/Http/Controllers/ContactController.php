<?php

namespace Modules\Theme\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;

class ContactController extends Controller
{
    public function index(){
        return view('front-end.layouts.lien-he');   
    }

    
    public function store(Request $request){
        $dataInsert = $request->all();
        Contact::create($dataInsert);
        return back();
    }

}
