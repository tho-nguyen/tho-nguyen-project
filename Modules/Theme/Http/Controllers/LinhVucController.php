<?php

namespace Modules\Theme\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Danhmuclinhvuc;
use App\Baivietlinhvuc;

class LinhVucController extends Controller
{

    public function index($slug){
        $category = Danhmuclinhvuc::where('slug', $slug)->first();
        $post     = Baivietlinhvuc::where('danhmuc_id',$category->id)->where('active',1)->orderBy('created_at','DESC')->get();
        return view('front-end.layouts.linh-vuc.bai-viet',compact('post','category'));
    }

    public function show($slug){

        $postDetail = Baivietlinhvuc::where('slug',$slug)->first();
        // GET POST
        $cate   = Danhmuclinhvuc::where('id', $postDetail->danhmuc_id)->first();
        // GET CATEGORY
        $countPostCate = Danhmuclinhvuc::withCount('baiviet')->get();
        // COUNT POST BY CATEGORY
        $postRecent = Baivietlinhvuc::where('active',1)->where('id','<>',$postDetail->id)->orderBy('created_at','DESC')->take(6)->get();
        // Get recent post
        return view('front-end.layouts.linh-vuc.bai-viet-chi-tiet',compact(['postDetail','cate','countPostCate','postRecent']));
    }
}
