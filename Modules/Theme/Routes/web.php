<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// TRANG CHU
Route::get('/', 'HomeController@index');
Route::post('/', 'HomeController@store')->name('home.store');


// VAT LIEU XAY DUNG
Route::get('/vat-lieu/{slug}', 'VatLieuController@index')->name('vatlieu');


// BAI VIET DU AN

Route::get('/du-an', 'DuAnController@index')->name('duan');
Route::get('/du-an/{slug}', 'DuAnController@show')->name('duan.show');

// BAI VIET BAT DONG SAN
Route::get('/bat-dong-san', 'BatDongSanController@index')->name('bds');
Route::get('/bat-dong-san/{slug}', 'BatDongSanController@show')->name('bds.show');

// BAI VIET  TIN TUC
Route::get('/tin-tuc/{slug}', 'NewsController@index')->name('tintuc');
Route::get('/tin-tuc/bai-viet-chi-tiet/{slug}', 'NewsController@show')->name('tintuc.show');


//  BAI VIET LINH VUC
Route::get('/linh-vuc/{slug}', 'LinhVucController@index')->name('linhvuc');

Route::get('/linh-vuc/bai-viet-chi-tiet/{slug}', 'LinhVucController@show')->name('linhvuc.show');

Route::get('/gioi-thieu', 'GioithieuController@index');

Route::get('/lien-he', 'ContactController@index');
Route::post('/lien-he/12', 'ContactController@store')->name("contact.post");




