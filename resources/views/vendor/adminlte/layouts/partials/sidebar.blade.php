<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                {!! Auth::user()->showAvatar(['class' => 'img-circle'], asset(config('settings.avatar_default'))) !!}
            </div>
            <div class="pull-left info">
                <p>Admin</p>
                <i class="fa fa-circle text-success"></i> Online
            </div>
        </div>
        <form method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Tìm kiếm..." />
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i
                            class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        @php
            
            $arLink = [
                [
                    'icon' => 'fas fa-tachometer-alt ',
                    'title' => __('Trang Chủ'),
                    'href' => 'admin',
                ],
                [
                    'icon' => 'fa fa-th',
                    'title' => __('Hệ Thống'),
                    'child' => [
                        [
                            'icon' => 'fas fa-cog',
                            'title' => __(' Cấu hình'),
                            'href' => 'admin/settings',
                            'permission' => 'SettingController',
                        ],
                        [
                            'icon' => 'fas fa-user',
                            'title' => __(' Tài khoản'),
                            'href' => 'admin/users',
                            'permission' => 'UsersController@index',
                        ],
                        [
                            'icon' => 'fas fa-user-lock',
                            'title' => __(' Vai trò'),
                            'href' => 'admin/roles',
                            'permission' => 'RolesController@index',
                        ],
                    ],
                ],
                [
                    'icon' => 'fas fa-paint-brush',
                    'title' => __(' Slider'),
                    'href' => 'admin/sliders',
                    'permission' => 'SliderController@index',
                    
                ],
                [
                    'icon' => 'fas fa-podcast',
                    'title' => __(' Liên hệ'),
                    'href' => 'admin/contacts',
                    'permission' => 'ContactController@index',
                    
                ],
                [
                    'icon' => 'fas fa-address-card mr-2',
                    'title' => __(' Giới thiệu'),
                    'href' => 'admin/gioithieu/1/edit',
                    'permission' => 'GioithieuController@show',
                ],
                // [
                //     'icon' => 'fas fa-address-card mr-2',
                //     'title' => __(' Yêu cầu tư vấn'),
                //     'href' => 'admin/yeucau',
                //     'permission' => 'YeuCauController@show',
                // ],
                [
                    'icon' => '',
                    'title' => __('Tin tức'),
                    'child' => [
                        [
                            'icon' => '',
                            'title' => __('Danh Mục'),
                            'href' => 'admin/categories',
                            'permission' => 'CategoryController@index',
                        ],
                        [
                            'icon' => '',
                            'title' => __('Bài viết'),
                            'href' => 'admin/news',
                            'permission' => 'ContactController@index',
                        ],
                        
                    ],
                ],
                [
                    'icon' => '',
                    'title' => __('Lĩnh vực hoạt động'),
                    'child' => [
                        [
                            'icon' => '',
                            'title' => __('Danh mục lĩnh vực'),
                            'href' => 'admin/linhvuc',
                            'permission' => 'LinhvucController@index',
                        ],
                        [
                            'icon' => '',
                            'title' => __('Bài viết'),
                            'href' => 'admin/baiviet',
                            'permission' => 'BaivietController@index',
                        ],
                        
                    ],
                ],
                [
                    'icon' => '',
                    'title' => __('Vật liệu xây dựng'),
                    'child' => [
                        [
                            'icon' => '',
                            'title' => __('Danh mục Vật liệu'),
                            'href' => 'admin/vatlieu',
                            'permission' => 'VatlieuController@index',
                        ],
                        [
                            'icon' => '',
                            'title' => __('Sản phẩm'),
                            'href' => 'admin/sanpham',
                            'permission' => 'SanPhamVatLieuController@index',
                        ],
                        
                    ],
                ],
                [
                    'icon' => '',
                    'title' => __('Dự án'),
                    'href' => 'admin/du-an',
                    'permission' => 'DuanController@index',
                ],
                [
                    'icon' => '',
                    'title' => __('Bất động sản'),
                    'href' => 'admin/batdongsan',
                    'permission' => 'BatDongSanController@index',
                ],
                
                
            ];
        @endphp
        {{ Menu::sidebar(Auth::user(), $arLink) }}
    </section>
</aside>
