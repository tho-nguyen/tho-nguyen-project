<head>
    <meta charset="UTF-8">
    <title> {{ strip_tags(Config("settings.app_logo")) }} - @yield('htmlheader_title', 'Your title here') </title>
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" />
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="theme-color" content="#ffffff">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="manifest" href="{{ asset('manifest.json') }}">
    <link href="{{ url (mix ('/css/all.css') ) }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome-iconpicker.css') }}" >
    @yield('css')
</head>
