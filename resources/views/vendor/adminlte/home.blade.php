@extends('adminlte::layouts.app')

@section('htmlheader_title')
Trang Chủ
@endsection
@section('contentheader_title')
Trang Chủ
<small>Bảng điều khiển</small>
@endsection
@section('breadcrumb')
<ol class="breadcrumb">
	<li><i class="fa fa-home"></i> Trang Chủ</li>
</ol>
@endsection
@section('main-content')
<div class="row">
	@can('UsersController@index')
	<div class="col-md-4">
		<!-- small box -->
		<div class="small-box bg-red">
			<div class="inner">
				<h3>{{ $usersCount }}</h3>
				<p>Tài khoản</p>
			</div>
			<div class="icon">
				<i class="fa fa-user-secret"></i>
			</div>
			<a href="{{ url('admin/users') }}" class="small-box-footer">Thông tin chi tiết <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	@endcan
	<div class="col-md-4">
		<!-- small box -->
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3>{{ $news }}</h3>

				<p>Tin tức</p>
			</div>
			<div class="icon">
				<i class="fa fa-building-o"></i>
			</div>
			<a href="{{ url('admin/news') }}" class="small-box-footer">Thông tin chi tiết <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>	
</div>
@endsection