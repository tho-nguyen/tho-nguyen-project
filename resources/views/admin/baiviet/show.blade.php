@extends('adminlte::layouts.app')
@section('htmlheader_title')
    Bài viết
@endsection
@section('contentheader_title')
    Bài viết
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-home"></i> Trang chủ</a></li>
        <li><a href="{{ url('/admin/news') }}">Bài viết</a></li>
        <li class="active">{{ __("Chi tiết") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __("Chi tiết") }}</h3>
            <div class="box-tools">
                <a href="{{ !empty($backUrl) ? $backUrl : url('/admin/baiviet') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ trans('Danh sách') }}</span></a>
                @can('NewsController@update')
                    <a href="{{ url('/admin/baiviet/' . $baiviet->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('Sữa') }}</span></a>
                @endcan
                @can('NewsController@destroy')
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['admin/baiviet', $baiviet->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('Xóa').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('Xóa'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th style="width: 15%;"> {{ trans('Tiêu đề') }} </th>
                    <td> {{ $baiviet->{'title'} }} </td>
                </tr>
                    <tr>
                        <th> {{ trans('URL') }} </th>
                        <td> <a href="">{{ $baiviet->{'slug'} }}</a> </td>
                    </tr>
                <tr>
                    <th> {{ trans('Danh mục') }} </th>
                    <td> {{ !empty($baiviet->linhvuc) ? $baiviet->linhvuc->{'title'} : '' }} </td>
                </tr>
                
                <tr>
                    <th> {{ trans('Ảnh') }} </th>
                    <td>
                        @if(!empty($baiviet->image))
                            <img width="100" src="{{ asset($baiviet->image) }}" alt="{{ $baiviet->{'title'} }}"/>
                        @endif
                    </td>
                </tr>
                <tr>
                    <th> {{ trans('Mô tả') }} </th>
                    <td>{{ $baiviet->{'description'} }}</td>
                </tr>
                <tr>
                    <th> {{ trans('Nội dung') }} </th>
                    <td> {!! htmlspecialchars_decode($baiviet->{'content'}) !!} </td>
                </tr>
                <tr>
                    <th> {{ trans('Ngày cập nhật') }} </th>
                    <td> {{ Carbon\Carbon::parse($baiviet->updated_at)->format(config('settings.format.datetime')) }} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection