@extends('adminlte::layouts.app')
@section('style')
<style>
.select2 {
    width: 250px;
}
</style>
@endsection
@section('htmlheader_title')
Bài viết
@endsection
@section('contentheader_title')
Bài viết
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ url("admin") }}"><i class="fa fa-home"></i> Trang chủ</a></li>
    <li class="active">Bài viết</li>
</ol>
@endsection
@section('main-content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Danh sách</h3>
        <div class="box-tools">
            {!! Form::open(['method' => 'GET', 'url' => '/admin/baiviet', 'class' => 'pull-left', 'role' => 'search']) !!}
            <div class="input-group" style="margin-right: 5px; display:flex;">
                <div class="select-group" style="margin-right: 5px;">
                    {!! Form::select('danhmuc_id', $linhvuc, \Request::get('danhmuc_id'), ['class' => 'form-control input-sm select2']) !!}
                </div>
                <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="Tìm kiếm" style="width: 250px; margin-right: 5px;">
                <button class="btn btn-default btn-sm" type="submit">
                    <i class="fa fa-search"></i> Tìm
                </button>
            </div>
            {!! Form::close() !!}
            @can('NewsController@store')
            <a href="{{ url('/admin/baiviet/create') }}" class="btn btn-success btn-sm" title="Thêm mới">
                <i class="fa fa-plus" aria-hidden="true"></i> <span class="hidden-xs">Thêm mới</span>
            </a>
            @endcan
        </div>
    </div>
    @php($index = ($baiviet->currentPage()-1)*$baiviet->perPage())
    <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover">
            <tbody>
                <tr class="bg-info">
                    <th class="text-center" style="width: 3.5%;">
                        <input type="checkbox" name="chkAll" id="chkAll" />
                    </th>
                    <th class="text-center" style="width: 3.5%">STT</th>
                    <th>@sortablelink('title',trans('Tiêu đề'))</th>
                    <th class="text-center" width="8%">{{ trans('Kích hoạt') }}</th>
                    <th>@sortablelink('updated_at',trans('Ngày tạo'))</th>
                    <th>@sortablelink('updated_at',trans('Ngày cập nhật'))</th>
                    <th style="width: 7%"></th>
                </tr>
                @foreach($baiviet as $item)
                <tr>
                    <td class="text-center">
                        <input type="checkbox" name="chkId" id="chkId" value="{{ $item->id }}" data-id="{{ $item->id }}" />
                    </td>
                    <td class="text-center">{{ ++$index }}</td>
                    @can('NewsController@show')
                    <td><a href="{{url('/admin/baiviet').'/'.$item->id}}" style="color: black;">{{ $item->{'title'} }}</a></td>
                    @endcan
                    <td class="text-center">{!! $item->active == config('settings.active') ? '<i class="fa fa-check text-primary"></i>' : '' !!}</td>
                    <td>{{ Carbon\Carbon::parse($item->created_at)->format(config('settings.format.date')) }}</td>
                    <td>{{ Carbon\Carbon::parse($item->updated_at)->format(config('settings.format.date')) }}</td>
                    <td style="display: flex">
                        @can('NewsController@show')
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/baiviet/' . $item->id, 'class' => 'pd-2']) !!}
                        <input type="hidden" name="back_url" value="{{ url()->full() }}">
                        {!! Form::button('<i class="fa fa-eye" aria-hidden="true"></i> ', array(
                        'type' => 'submit',
                        'class' => 'btn btn-info btn-xs',
                        'style' => 'margin-right: 5px',
                        'title' => __('message.view')
                        )) !!}
                        {!! Form::close() !!}
                        @endcan
                        @can('NewsController@update')
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/baiviet/'. $item->id . '/edit', 'class' => 'pd-2']) !!}
                        <input type="hidden" name="back_url" value="{{ url()->full() }}">
                        {!! Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> ', array(
                        'type' => 'submit',
                        'class' => 'btn btn-primary btn-xs',
                        'style' => 'margin-right: 5px',
                        'title' => __('Sữa')
                        )) !!}
                        {!! Form::close() !!}
                        @endcan
                        @can('NewsController@destroy')
                        {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['/admin/baiviet', $item->id],
                        'class' => 'pd-2'
                        ]) !!}
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => __('Xóa'),
                        'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                        )) !!}
                        {!! Form::close() !!}
                        @endcan
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="box-footer clearfix">
        <div id="btn-act">
            @can('NewsController@destroy')
            <a href="#" id="deleteNews" data-action="deleteNews" class="btn-act btn btn-danger btn-sm" title="{{ __('Xóa') }}">
                <i class="fa fa-trash-o" aria-hidden="true"></i>
            </a>
            @endcan
            @can('NewsController@active')
            <a href="#" id="activeNews" data-action="activeNews" class="btn-act btn btn-success btn-sm" title="{{ __('message.approved') }}">
                <i class="fa fa-check" aria-hidden="true"></i>
            </a>
            @endcan
        </div>
        <div class="page-footer pull-right">
            {!! $baiviet->appends(\Request::except('page'))->render() !!}
        </div>
    </div>
</div>
@endsection
@section('scripts-footer')
@toastr_js
@toastr_render
<script type="text/javascript">
    $(function() {
        $('#chkAll').on('click', function() {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });
    });
    $('#btn-act').on('click', '.btn-act', function(e) {
        e.preventDefault();
        let action = $(this).data('action');
        ajaxCategory(action);
    });

    function ajaxCategory(action) {
        let chkId = $("input[name='chkId']:checked");
        let actTxt = '',
            successAlert = '',
            classAlert = '';
        switch (action) {
            case 'activeNews':
                actTxt = 'duyệt';
                classAlert = 'alert-success';
                break;
            case 'deleteNews':
                actTxt = 'xóa';
                classAlert = 'alert-danger';
                break;
        }
        if (chkId.length != 0) {
            let notificationConfirm = 'Bạn có muốn ' + actTxt + ' tin tức này không?';
            let notification = confirm(notificationConfirm);
            if (notification) {
                var arrId = '';
                $("input[name='chkId']:checked").map((val, key) => {
                    arrId += key.value + ',';
                });
                axios.get('{{url('/ajax')}}/' + action, {
                        params: {
                            ids: arrId
                        }
                    })
                    .then((response) => {
                        if (response.data.success === 'ok') {
                            location.reload(true);
                        }
                    })
                    .catch((error) => {})
            }
        } else {
            let notificationAlert = 'Vui lòng chọn tin tức để ' + actTxt + '!';
            toastr.error(notificationAlert);
        }
    }
</script>
@endsection