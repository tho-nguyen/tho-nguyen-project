@extends('adminlte::layouts.app')
@section('htmlheader_title')
    Thông tin admin
@endsection
@section('contentheader_title')
    Thông tin admin
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-home"></i> Trang chủ</a></li>
        <li class="active">Thông tin admin</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('Sửa') }}</h3>
        </div>

        {!! Form::model($user, [
            'method' => 'POST',
            'url' => ['/profile'],
            'files' => true,
            'class' => 'form-horizontal'
        ]) !!}

        @include ('admin.users.form-profile', ['submitButtonText' => __('Cập nhật'), 'isProfile'=> true])

        {!! Form::close() !!}
    </div>
@endsection