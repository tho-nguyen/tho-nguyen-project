@extends('adminlte::layouts.app')
@section('htmlheader_title')
    Tài khoản
@endsection
@section('contentheader_title')
    Tài khoản
@endsection
@section('contentheader_description')
    
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-home"></i> Trang chủ</a></li>
        <li><a href="{{ url('/admin/users') }}">Tài khoản</a></li>
        <li class="active">{{ __('Chỉnh sửa') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('Chỉnh sửa') }}</h3>
            <div class="box-tools">
                <a href="{{ url('/admin/users') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">Danh sách</span></a>
            </div>
        </div>

        {!! Form::model($user, [
            'method' => 'PATCH',
            'url' => ['/admin/users', $user->id],
            'files' => true,
            'class' => 'form-horizontal'
        ]) !!}

        @include ('admin.users.form', ['submitButtonText' => __('Cập nhật')])

        {!! Form::close() !!}
    </div>
@endsection