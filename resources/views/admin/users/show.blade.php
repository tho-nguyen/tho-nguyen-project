@extends('adminlte::layouts.app')
@section('htmlheader_title')
    Tài khoản
@endsection
@section('contentheader_title')
    Tài khoản
@endsection
@section('contentheader_description')
    
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-home"></i> Trang chủ</a></li>
        <li><a href="{{ url('/admin/users') }}">Tài khoản</a></li>
        <li class="active">{{ __('Chi tiết') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __('Chi tiết') }}</h3>
            <div class="box-tools">
                <a href="{{ url('/admin/users') }}" title="Back" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">Danh sách</span></a>
                @can('UsersController@update')
                <a href="{{ url('/admin/users/' . $user->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('Sữa') }}</span></a>
                @endcan
                @can('UsersController@destroy')
                {!! Form::open([
                    'method' => 'DELETE',
                    'url' => ['/admin/users', $user->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'. __('Xóa') .'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Xoá',
                            'onclick'=>'return confirm("'. __('message.confirm_delete') .'?")'
                    ))!!}
                {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th>{{ __('Kích hoạt') }}</th>
                        <td>{{ $user->active==Config("settings.active")?__('message.yes'):__('message.no') }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Ảnh đại diện') }}</th>
                        <td>{!! $user->showAvatar() !!}</td>
                    </tr>

                    <tr>
                        <th>{{ __('Họ và tên') }}</th>
                        <td>{{ $user->name }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Tên đăng nhập') }}</th>
                        <td>{{ $user->username }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Email') }}</th>
                        <td>{{ $user->email }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Phân quyền') }}</th>
                        <td>
                            @foreach ($user->roles as $index=>$role)
                                <span class="badge label-{{ $role->color }}">{{ $role->label }}</span>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>{{ __('Giới tính') }}</th>
                        <td>{{ isset($user->profile)?$user->profile->textGender:'' }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('SDT') }}</th>
                        <td>{{ isset($user->profile)?$user->profile->phone:'' }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Địa chỉ') }}</th>
                        <td>{{ isset($user->profile)?$user->profile->address:'' }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Ngày sinh') }}</th>
                        <td>{{ isset($user->profile)?Carbon\Carbon::parse($user->profile->birthday)->format(config('settings.format.date')):"" }}</td>
                    </tr>
                    <tr>
                        <th>{{ __('Chức vụ') }}</th>
                        <td>{{ isset($user->profile)?$user->profile->position:'' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection