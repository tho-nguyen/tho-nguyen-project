@extends('adminlte::layouts.app')
@section('htmlheader_title')
    Danh mục vật liệu 
@endsection
@section('contentheader_title')
    Danh mục vật liệu 
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-home"></i> Trang chủ</a></li>
        <li><a href="{{ url('/admin/vatlieu') }}">Danh mục vật liệu </a></li>
        <li class="active">{{ __('Sửa') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('Sửa') }}</h3>
            <div class="box-tools">
                <a href="{{ url('/admin/vatlieu') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">Danh sách</span></a>
            </div>
        </div>

        {!! Form::model($vatlieu_cate, [
            'method' => 'PATCH',
            'url' => ['/admin/vatlieu', $vatlieu_cate->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}

        @include ('admin.vatlieu.form', ['submitButtonText' => __('Cập nhật')])

        {!! Form::close() !!}
    </div>
@endsection