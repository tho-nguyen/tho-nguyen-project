@extends('adminlte::layouts.app')
@section('htmlheader_title')
    Tin tức
@endsection
@section('contentheader_title')
    Tin tức
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-home"></i> Trang chủ</a></li>
        <li><a href="{{ url('/admin/news') }}">Tin tức</a></li>
        <li class="active">Thêm mới</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Thêm mới</h3>
            <div class="box-tools">
                <a href="{{ url('/admin/news') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">Danh sách</span></a>
            </div>
        </div>

        {!! Form::open(['url' => '/admin/news', 'class' => 'form-horizontal', 'files' => true]) !!}

        @include('admin.news.form')

        {!! Form::close() !!}
    </div>
@endsection