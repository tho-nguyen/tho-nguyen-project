@extends('adminlte::layouts.app')
@section('htmlheader_title')
Danh mục tin tức 
@endsection
@section('contentheader_title')
Danh mục tin tức 
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ url("admin") }}"><i class="fa fa-home"></i> Trang chủ</a></li>
    <li><a href="{{ url('/admin/categories') }}">Danh mục tin tức </a></li>
    <li class="active">{{ __("Chi tiết") }}</li>
</ol>
@endsection
@section('main-content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">{{ __("Chi tiết") }}</h3>
        <div class="box-tools">
            <a href="{{ url('/admin/categories') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ trans('Danh sách') }}</span></a>
            @can('CategoryController@update')
            <a href="{{ url('/admin/categories/' . $category->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('Sữa') }}</span></a>
            @endcan
            @can('CategoryController@destroy')
            {!! Form::open([
            'method'=>'DELETE',
            'url' => ['admin/categories', $category->id],
            'style' => 'display:inline'
            ]) !!}
            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('Xóa').'</span>', array(
            'type' => 'submit',
            'class' => 'btn btn-danger btn-sm',
            'title' => __('Xóa'),
            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
            ))!!}
            {!! Form::close() !!}
            @endcan
        </div>
    </div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-striped">
            <tbody>
                <tr>
                    <th> {{ trans('Tiêu đề') }} </th>
                    <td> {{ $category->{'title'} }} </td>
                </tr>
                <tr>
                    <th> {{ trans('URL') }} </th>
                    <td> <a href="">{{ $category->{'slug'} }}</a> </td>

                </tr>
                <tr>
                    <th> {{ trans('Mô tả') }} </th>
                    {{-- <td>{{ $category->description }}</td> --}}
                    <td>{!!html_entity_decode($category->description)!!}</td>
                </tr>
                <tr>
                    <th> {{ trans('Ngày cập nhật') }} </th>
                    <td> {{ Carbon\Carbon::parse($category->updated_at)->format(config('settings.format.datetime')) }} </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection