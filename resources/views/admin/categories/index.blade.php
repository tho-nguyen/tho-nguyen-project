@extends('adminlte::layouts.app')
@section('htmlheader_title')
Danh mục tin tức
@endsection
@section('contentheader_title')
Danh mục tin tức
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ url("admin") }}"><i class="fa fa-home"></i> Trang chủ</a></li>
    <li class="active">Danh mục tin tức</li>
</ol>
@endsection
@section('main-content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Danh sách</h3>
        <div class="box-tools">
            {!! Form::open(['method' => 'GET', 'url' => '/admin/categories', 'class' => 'pull-left', 'role' => 'search']) !!}
            <div class="input-group" style="width: 200px;">
                <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="Tìm kiếm" style="width: 250px;">
                <span class="input-group-btn">
                    <button class="btn btn-default btn-sm" type="submit">
                        <i class="fa fa-search"></i> Tìm
                    </button>
                </span>
            </div>
            {!! Form::close() !!}
            @can('CategoryController@store')
            <a href="{{ url('/admin/categories/create') }}" class="btn btn-success btn-sm" title="Thêm mới">
                <i class="fa fa-plus" aria-hidden="true"></i> <span class="hidden-xs">Thêm mới</span>
            </a>
            @endcan
        </div>
    </div>
    @php($index = ($categories->currentPage()-1)*$categories->perPage())
    <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover">
            <tbody>
                <tr class="bg-info">   
                    <th class="text-center" style="width: 3.5%;">
                        <input type="checkbox" name="chkAll" id="chkAll" />
                    </th>
                    <th class="text-center" style="width: 3.5%">STT</th>                 
                    <th>Tiêu đề</th>
                    <th>Slug</th>
                    <th>Mô tả</th>
                    <th class="text-center">Duyệt</th>
                    <th class="text-center">Ngày cập nhật</th>
                    <th style="width: 7%;"></th>
                </tr>
                @foreach($categories as $item)
                <tr>
                    <td class="text-center">
                        <input type="checkbox" name="chkId" id="chkId" value="{{ $item->id }}" data-id="{{ $item->id }}" />
                    </td>
                    <td class="text-center">{{ ++$index }}</td>
                    @can('NewCategoryController@show')
                    <td><a href="{{url('/admin/categories').'/'.$item->id}}" style="color: black;">{{ $item->{'title'} }}</a></td>
                    @endcan
                    <td class="text-center">{{  $item->{'slug'} }}</td>
                    
                    <td class="text-center">{!!html_entity_decode(Str::limit($item['description'], 50))!!}</td>
                    <td class="text-center">{!! $item->active == config('settings.active') ? '<i class="fa fa-check text-primary"></i>' : '' !!}</td>
                    <td>{{ Carbon\Carbon::parse($item->updated_at)->format(config('settings.format.date')) }}</td>
                    <td style="display: flex">
                        @can('NewCategoryController@show')
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/categories/' . $item->id, 'class' => 'pd-2']) !!}
                        <input type="hidden" name="back_url" value="{{ url()->full() }}">
                        {!! Form::button('<i class="fa fa-eye" aria-hidden="true"></i> ', array(
                        'type' => 'submit',
                        'class' => 'btn btn-info btn-xs',
                        'style' => 'margin-right: 5px',
                        'title' => __('message.view')
                        )) !!}
                        {!! Form::close() !!}
                        @endcan
                        @can('NewCategoryController@update')
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/categories/'. $item->id . '/edit', 'class' => 'pd-2']) !!}
                        <input type="hidden" name="back_url" value="{{ url()->full() }}">
                        {!! Form::button('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> ', array(
                        'type' => 'submit',
                        'class' => 'btn btn-primary btn-xs',
                        'style' => 'margin-right: 5px',
                        'title' => __('Sữa')
                        )) !!}
                        {!! Form::close() !!}
                        @endcan
                        @can('NewCategoryController@destroy')
                        {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['/admin/categories', $item->id],
                        'class' => 'pd-2'
                        ]) !!}
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => __('Xóa'),
                        'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                        )) !!}
                        {!! Form::close() !!}
                        @endcan
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="box-footer clearfix">
            {!! $categories->appends(\Request::except('page'))->render() !!}
    </div>
</div>
@endsection