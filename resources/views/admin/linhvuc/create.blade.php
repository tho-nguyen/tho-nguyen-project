@extends('adminlte::layouts.app')
@section('htmlheader_title')
Danh mục lĩnh vực 
@endsection
@section('contentheader_title')
Danh mục lĩnh vực 
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ url("admin") }}"><i class="fa fa-home"></i> Trang chủ</a></li>
    <li><a href="{{ url('/admin/linhvuc') }}">Danh mục lĩnh vực </a></li>
    <li class="active">Thêm mới</li>
</ol>
@endsection

@section('main-content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Thêm mới</h3>
        <div class="box-tools">
            <a href="{{ url('/admin/linhvuc') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">Danh sách</span></a>
        </div>
    </div>

    {!! Form::open(['url' => '/admin/linhvuc', 'class' => 'form-horizontal', 'files' => true]) !!}

    @include('admin.linhvuc.form')

    {!! Form::close() !!}
</div>
@endsection