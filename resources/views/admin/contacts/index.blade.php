@extends('adminlte::layouts.app')
@section('htmlheader_title')
    Liên hệ
@endsection
@section('contentheader_title')
    Liên hệ
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-home"></i> Trang chủ</a></li>
        <li class="active">Liên hệ</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Danh sách</h3>
            <div class="box-tools">
                {!! Form::open(['method' => 'GET', 'url' => '/contacts', 'class' => 'pull-left', 'role' => 'search'])  !!}
                <div class="input-group" style="width: 200px;">
                    <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="Tìm kiếm">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="submit">
                            <i class="fa fa-search"></i> Tìm
                        </button>
                    </span>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @php($index = ($contacts->currentPage()-1)*$contacts->perPage())
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th class="text-center">STT</th>
                    <th>Họ tên</th>
                    <th>Email</th>
                    <th>Nội dung</th>
                    <th>Ngày cập nhật </th>
                    <th></th>
                </tr>
                @foreach($contacts as $item)
                    <tr>
                        <td class="text-center">{{ ++$index }}</td>
                        <td>{{ $item->fullname }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->message }}</td>
                        <td>{{ Carbon\Carbon::parse($item->updated_at)->format(config('settings.format.datetime')) }}</td>
                        <td>
                            <a href="{{ url('/admin/contacts/' . $item->id) }}" title="{{ __('message.view') }}"><button
                                class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i>
                               </button></a>
                            {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/admin/contacts', $item->id],
                            'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ',
                            array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => __('Xóa'),
                            'onclick'=>'return confirm("Bạn chắc chắn xóa không ?")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="box-footer clearfix">
                {!! $contacts->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection
