@extends('adminlte::layouts.app')
@section('htmlheader_title')
    Liên hệ
@endsection
@section('contentheader_title')
    Liên hệ
@endsection
@section('contentheader_description')
    
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-home"></i> {{ __('Dashboard') }}</a></li>
        <li><a href="{{ url('/yeucau') }}">Liên hệ</a></li>
        <li class="active">{{ __('Chi tiết') }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __('Chi tiết') }}</h3>
            <div class="box-tools">
                <a href="{{ url('admin/yeucau') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">Danh sách</span></a>
                @can('ContactController@destroy')
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['admin/contacts', $yeucau->id],
                    'style' => 'display:inline'
                ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('Xóa').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Xoá',
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th> Họ tên </th>
                    <td> {{ $yeucau->name }} </td>
                </tr>
                <tr>
                    <th> Phone </th>
                    <td> {{ $yeucau->phone }} </td>
                </tr>
                <tr>
                    <th> Mô tả </th>
                    <td> {{ $yeucau->description }} </td>
                </tr>
            
                <tr>
                    <th> Ngày cập nhật </th>
                    <td> {{ Carbon\Carbon::parse($yeucau->updated_at)->format(config('settings.format.datetime')) }} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection