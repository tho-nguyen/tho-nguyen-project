@extends('adminlte::layouts.app')
@section('htmlheader_title')
    Yêu cầu tư vấn
@endsection
@section('contentheader_title')
    Yêu cầu tư vấn
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-home"></i> Trang chủ</a></li>
        <li class="active">Yêu cầu tư vấn</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Danh sách</h3>
            <div class="box-tools">
                {!! Form::open(['method' => 'GET', 'url' => 'admin/yeucau', 'class' => 'pull-left', 'role' => 'search'])  !!}
                <div class="input-group" style="width: 200px;">
                    <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="Tìm kiếm">
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="submit">
                            <i class="fa fa-search"></i> Tìm
                        </button>
                    </span>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @php($index = ($yeucau->currentPage()-1)*$yeucau->perPage())
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th class="text-center">STT</th>
                    <th>Họ tên</th>
                    <th>phone</th>
                    <th>Mô tả</th>
                    <th>Ngày cập nhật </th>
                    <th></th>
                </tr>
                @foreach($yeucau as $item)
                    <tr>
                        <td class="text-center">{{ ++$index }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->phone }}</td>
                        <td>{{ $item->description }}</td>
                        <td>{{ Carbon\Carbon::parse($item->updated_at)->format(config('settings.format.datetime')) }}</td>
                        <td>
                            <a href="{{ url('/admin/yeucau/' . $item->id) }}" title="{{ __('message.view') }}"><button
                                class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i>
                               </button></a>
                            {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/admin/yeucau', $item->id],
                            'style' => 'display:inline'
                            ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ',
                            array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => __('Xóa'),
                            'onclick'=>'return confirm("Bạn chắc chắn xóa không ?")'
                            )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="box-footer clearfix">
                {!! $yeucau->appends(\Request::except('page'))->render() !!}
            </div>
        </div>
    </div>
@endsection
