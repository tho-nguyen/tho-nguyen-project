@extends('adminlte::layouts.app')
@section('htmlheader_title')
    Sản phẩm
@endsection
@section('contentheader_title')
    Sản phẩm
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-home"></i> Trang chủ</a></li>
        <li><a href="{{ url('/admin/sanpham') }}">Sản phẩm</a></li>
        <li class="active">Thêm mới</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Thêm mới</h3>
            <div class="box-tools">
                <a href="{{ url('/admin/sanpham') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">Danh sách</span></a>
            </div>
        </div>

        {!! Form::open(['url' => '/admin/sanpham', 'class' => 'form-horizontal', 'files' => true]) !!}

        @include('admin.sanpham.form')

        {!! Form::close() !!}
    </div>
@endsection