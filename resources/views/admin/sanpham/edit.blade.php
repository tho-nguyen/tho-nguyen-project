@extends('adminlte::layouts.app')
@section('htmlheader_title')
    Sản phẩm
@endsection
@section('contentheader_title')
    Sản phẩm
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("admin") }}"><i class="fa fa-home"></i> Trang chủ</a></li>
        <li><a href="{{ url('/admin/sanpham') }}">Sản phẩm</a></li>
        <li class="active">{{ __('Sửa') }}</li>
    </ol>
@endsection

@section('main-content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('Sửa') }}</h3>
            <div class="box-tools">
                <a href="{{ !empty($backUrl) ? $backUrl : url('/admin/sanpham') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">Danh sách</span></a>
            </div>
        </div>

        {!! Form::model($sanpham, [
            'method' => 'PATCH',
            'url' => ['/admin/sanpham', $sanpham->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}

        @include ('admin.sanpham.form', ['submitButtonText' => __('Cập nhật')])

        {!! Form::close() !!}
    </div>
@endsection