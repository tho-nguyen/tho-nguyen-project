@extends('adminlte::layouts.app')
@section('htmlheader_title')
Slider
@endsection
@section('contentheader_title')
Slider
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ url("admin") }}"><i class="fa fa-home"></i> Trang chủ</a></li>
    <li><a href="{{ url('/admin/sliders') }}">{{ __('Slider') }}</a></li>
    <li class="active">Thêm mới</li>
</ol>
@endsection

@section('main-content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Thêm mới</h3>
        <div class="box-tools">
            <a href="{{ url('/admin/sliders') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left"
                    aria-hidden="true"></i> <span class="hidden-xs">Danh sách</span></a>
        </div>
    </div>

    {!! Form::open(['url' => '/admin/sliders', 'class' => 'form-horizontal', 'files' => true]) !!}

    @include('admin.sliders.form')

    {!! Form::close() !!}
</div>
@endsection