@extends('adminlte::layouts.app')
@section('htmlheader_title')
Slider
@endsection
@section('contentheader_title')
Slider
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{{ url("admin") }}"><i class="fa fa-home"></i> Trang chủ</a></li>
    <li class="active">Slider</li>
</ol>
@endsection
@section('main-content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Danh sách</h3>
        <div class="box-tools">
            {!! Form::open(['method' => 'GET', 'url' => '/admin/sliders', 'class' => 'pull-left', 'role' => 'search']) !!}
            <div class="input-group" style="width: 200px;">
                <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search"
                    placeholder="Tìm kiếm">
                <span class="input-group-btn">
                    <button class="btn btn-default btn-sm" type="submit">
                        <i class="fa fa-search"></i> Tìm
                    </button>
                </span>
            </div>
            {!! Form::close() !!}
            @can('SliderController@store')
            <a href="{{ url('/admin/sliders/create') }}" class="btn btn-success btn-sm"
                title="Thêm mới">
                <i class="fa fa-plus" aria-hidden="true"></i> <span class="hidden-xs">Thêm mới</span>
            </a>
            @endcan
        </div>
    </div>
    @php($index = ($sliders->currentPage()-1)*$sliders->perPage())
    <div class="box-body table-responsive no-padding">
        <table class="table table-striped">
            <tbody>
                <tr>
                    <th class="text-center">STT</th>
                    <th class="text-center">Image</th>
                    <th>Tên</th>
                    <th>Đường dẫn URL</th>
                    <th class="text-center">Duyệt</th>
                    <th>@sortablelink('Ngày cập nhật ')</th>
                    <th></th>
                </tr>
                @foreach($sliders as $item)
                <tr>
                    <td class="text-center">{{ ++$index }}</td>
                    <td class="text-center" style="width: 10%"><img width="100" src="{{ asset($item->image) }}" alt="{{ $item->title }}" /></a></td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->link }}</td>
                    <td class="text-center">{!! $item->active == config('settings.active') ? '<i class="fa fa-check text-primary"></i>' : ''  !!}</td>
                    <td>{{ Carbon\Carbon::parse($item->updated_at)->format(config('settings.format.datetime')) }}</td>
                    <td>
                        @can('SliderController@show')
                        <a href="{{ url('/admin/sliders/' . $item->id) }}" title="{{ __('message.view') }}"><button
                                class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i>
                               </button></a>
                        @endcan
                        @can('SliderController@update')
                        <a href="{{ url('/admin/sliders/' . $item->id . '/edit') }}"
                            title="{{ __('Sữa') }}"><button class="btn btn-primary btn-xs"><i
                                    class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </button></a>
                        @endcan
                        @can('SliderController@destroy')
                        {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['/admin/sliders', $item->id],
                        'style' => 'display:inline'
                        ]) !!}
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> ',
                        array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => __('Xóa'),
                        'onclick'=>'return confirm("Bạn chắc chắn xóa không ?")'
                        )) !!}
                        {!! Form::close() !!}
                        @endcan
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="box-footer clearfix">
            {!! $sliders->appends(\Request::except('page'))->render() !!}
        </div>
    </div>
</div>
@endsection