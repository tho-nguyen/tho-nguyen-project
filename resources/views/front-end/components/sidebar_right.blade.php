@php
    $email = \App\Setting::where('key','company_email')->first();   
    $phone = \App\Setting::where('key','company_phone')->first();   
    $logo = \App\Setting::where('key','company_logo')->first();       
    $name = \App\Setting::where('key','company_website')->first();       
    $address = \App\Setting::where('key','company_address')->first();       
    $link = \App\Setting::where('key','company_link')->first();   
    //
    $news = \App\News::where('active',1)->orderBy('created_at','DESC')->take(5)->get();
    // 
    $cateLinhVuc = \App\Danhmuclinhvuc::withCount('baiviet')->orderBy('created_at','DESC')->take(4)->get(); 
    // 
    $cateProduct = \App\VatLieu::withCount('sanpham')->orderBy('created_at','DESC')->take(4)->get(); 

@endphp

<div class="widget">
    <div class="widget-box info">
        <h4 class="widget-title">Thông tin doanh nghiệp</h4>
        <div class="divider"></div>
        <div class="text-center">
            <img src="{{ asset($logo->value) }}" class="rounded-circle" width="100px" height="100px"  alt="">
        </div>
        <h6 class="fg-primary my-4 text-center"> {{ $name->value }} </h6>
        <ul class="categories my-4">
            <li><a href="#!"> <i class="fas fa-map-marker-alt w-10"></i> {{ $address->value }} </a></li>
            <li><a href="#!"> <i class="fas fa-phone-volume w-10"></i> {{ $phone->value }} </a></li>
            <li><a href="#!"> <i class="fas fa-paper-plane w-10"></i> {{ $email->value }} </a></li>
            <li><a href="#!"> <i class="fas fa-globe-europe w-10"></i> {{ $link->value }} </a></li>
        </ul>
    </div>

    <div class="widget-box">
        <h4 class="widget-title">Danh mục lĩnh vực</h4>
        <div class="divider"></div>
        <ul class="categories">
            @foreach($cateLinhVuc as $linhvuc_item)
                <li><a href="{{ route('linhvuc',['slug'=>$linhvuc_item->slug]) }}"> {{ $linhvuc_item->title }}  <span> {{ $linhvuc_item->baiviet_count }} </span></a></li>
            @endforeach
        </ul>
    </div>

    <div class="widget-box">
        <h4 class="widget-title">Danh mục sản phẩm</h4>
        <div class="divider"></div>
        <ul class="categories">
            @foreach($cateProduct as $cate_item)
                <li><a href="{{ route('vatlieu',['slug' => $cate_item->slug]) }}"> {{ $cate_item->title }}  <span> {{ $cate_item->sanpham_count }} </span></a></li>
            @endforeach
        </ul>
    </div>

    <div class="widget-box">
        <h4 class="widget-title">Tin Mới</h4>
        <div class="divider"></div>
        @foreach( $news as $new_item)
            
            <div class="blog-item">
                <div class="content">
                    <h6 class="post-title"><a href="{{ route('tintuc.show',['slug'=>$new_item->slug])}}">{{ $new_item->title }}</a></h6>
                    <div class="meta">
                        <a href="#"><span class="mai-calendar"></span> July 12, 2018</a>
                        <a href="#"><span class="mai-person"></span> Admin</a>
                        <a href="#"><span class="mai-chatbubbles"></span> 19</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>



    <div class="widget-box">
        <h3 class="widget-title">Lời cám ơn</h3>
        <div class="divider"></div>
        <p>
            Chúng tôi thực sự biết ơn bạn vì đã chọn chúng tôi làm nhà cung cấp dịch vụ và cho chúng tôi cơ hội phát triển. Không có thành tựu nào của chúng tôi có thể đạt được nếu không có bạn và sự hỗ trợ vững chắc của bạn.
        </p>
    </div>
</div>
