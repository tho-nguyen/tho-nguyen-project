<form action="{{ $route }}" method="POST" class="form-contact">
    @csrf
    <div class="row">
        <div class="col-sm-6 py-2">
            <label for="name" class="fg-grey">Họ và tên</label>
            <input type="text" name="fullname" class="form-control"  required placeholder="Nhập họ và tên">
        </div>
        <div class="col-sm-6 py-2">
            <label for="email" class="fg-grey">Email</label>
            <input type="email" name="email" class="form-control" required placeholder="Nhập địa chỉ email">
        </div>
        <div class="col-8 py-2">
            <label for="subject" class="fg-grey">Địa chỉ</label>
            <input type="text" name="address" class="form-control"  required placeholder="Nhập địa chỉ">
        </div>
        <div class="col-4 py-2">
            <label for="subject" class="fg-grey">Số điện thoại</label>
            <input type="number" name="phone" class="form-control"  required placeholder="Nhập số điện thoại">
        </div>
        <div class="col-12 py-2">
            <label for="message" class="fg-grey">Nội dung yêu cầu</label>
            <textarea id="message" name="message" rows="8" class="form-control" required placeholder="Nhập nội dung yêu cầu"></textarea>
        </div>
        <!-- Modal -->
        <div class="col-12 mt-3">
            <button class="form-submit btn btn-primary px-5">Đồng ý</button>
        </div>
    </div>
</form>
