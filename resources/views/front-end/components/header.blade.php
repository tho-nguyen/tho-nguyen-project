<!-- Back to top button -->
<div class="back-to-top"></div>
@php
    
    $email = \App\Setting::where('key','company_email')->first();   
    $phone = \App\Setting::where('key','company_phone')->first();   
    $logo = \App\Setting::where('key','company_logo')->first();   
@endphp

<header>
    <div class="top-bar">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-8 d-flex">
                    <div class="d-flex align-items-center">
                        <span class="mai-mail fg-primary mr-2"></span> 
                        <a href="mailto:{{ $email->value }}">{{ $email->value }}</a>
                    </div>
                    <div class="d-flex align-items-center ml-5">
                        <span class="mai-call fg-primary mr-2"></span> 
                        <a href="tel:+{{ $phone->value }}">+{{ $phone->value }}</a>
                    </div>
                </div>
                <div class="col-md-4 text-right d-none d-md-block">
                    <div class="social-mini-button">
                        <a href="#"><span class="mai-logo-facebook-f"></span></a>
                        <a href="#"><span class="mai-logo-twitter"></span></a>
                        <a href="#"><span class="mai-logo-youtube"></span></a>
                        <a href="#"><span class="mai-logo-linkedin"></span></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- .container -->
    </div>
    <!-- .top-bar -->


    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <a href="{{ url('/') }}" class="navbar-brand">
                <img src="{{ asset($logo->value) }}" class="rounded-circle" width="70px" height="70px" alt="">
            </a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

            <div class="navbar-collapse collapse" id="navbarContent">
                <ul class="navbar-nav ml-auto pt-3 pt-lg-0">
                    <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                        <a href="/" class="nav-link">Trang chủ</a>
                    </li>
                    <li class="nav-item {{ Request::is('gioi-thieu') ? 'active' : '' }}">
                        <a href="{{ url('gioi-thieu') }}" class="nav-link">Giới Thiệu</a>
                    </li>
                    {{-- START DANH MUC LINH VUC --}}
                    <li class="nav-item">
                        <a href="#!" class="nav-link">Lĩnh vực hoạt động</a>
                        <ul class="sub-nav">
                            @foreach($categoriesLinhVuc = \App\Danhmuclinhvuc::get() as $key)
                                <li>
                                    <a href="{{ route('linhvuc',['slug' => $key->slug]) }}" class="nav-link"> {{ $key->title }} </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    {{-- /END DANH MUC LINH VUC  --}}
                    <li class="nav-item {{ Request::is('du-an') ? 'active' : '' }}">
                        <a href="{{ route('duan') }}" class="nav-link">Dự án</a>
                    </li>
                    {{-- START VAT LIEU XAY DUNG --}}
                    <li class="nav-item ">
                        <a href="#!" class="nav-link">Vật liệu xây dựng</a>
                        <ul class="sub-nav">
                            @foreach($categoriesVatLieu = \App\Vatlieu::get() as $key)
                                <li>
                                    <a href="{{ route('vatlieu',['slug' => $key->slug]) }}" class="nav-link"> {{ $key->title }} </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    {{-- /END VAT LIEU XAY DUNG --}}
                    <li class="nav-item {{ Request::is('bat-dong-san') ? 'active' : '' }}">
                        <a href="{{ route('bds') }}" class="nav-link">Bất động sản</a>
                    </li>
                    {{-- START DANH MUC TIN TUC --}}
                    <li class="nav-item">
                        <a href="#!" class="nav-link">Tin Tức</a>
                        <ul class="sub-nav">
                            @foreach($categoriesNews = \App\Category::get() as $key)
                                <li>
                                    <a href="{{ route('tintuc',['slug' => $key->slug]) }}" class="nav-link"> {{ $key->title }} </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    {{-- /END DANH MUC TIN TUC  --}}
                    <li class="nav-item {{ Request::is('lien-he') ? 'active' : '' }}">
                        <a href="{{ url('lien-he') }}" class="nav-link">Liên hệ</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- .container -->
    </nav>
    <!-- .navbar -->

</header>