@extends('front-end.app')

@section('content')

@include('front-end.components.intro',['category'=>'Dự án hoàn thành','introName'=> 'Dự án']);

<main>
    <div class="page-section">
        <div class="container-fluid">
            <div class="row my-5">
                <div class="col-md-12 col-lg-8 mt-3">
                    <div class="row ">
                        @foreach($projects as $project_item)
                    
                        <div class="col-md-6 col-lg-4">
                            <div class="card-blog">
                                <img src="{{ asset($project_item->image)}}" width="100%" alt="">
                                <div class="body mt-3">
                                    <div class="post-title">
                                        <a class="d-2-lines" href="{{ route('duan.show',['slug'=>$project_item->slug]) }}">
                                            {{ $project_item->title }}
                                        </a>
                                    </div>
                                    <div class="post-excerpt d-3-lines">
                                        {{ $project_item->description }}
                                    </div>
                                </div>
                                <div class="footer">
                                    <a href="{{ route('duan.show',['slug'=>$project_item->slug]) }}">Xem thêm <span class="mai-chevron-forward text-sm"></span></a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-4">
                    @include('front-end.components.sidebar_right')
                </div>
            </div>
        </div>
        <!-- .container -->
    </div>
</main>
<!-- .page Tin Tuc Noi Bat -->


@endsection