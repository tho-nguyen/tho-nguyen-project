@extends('front-end.app')

@section('content')
<main>
    <div class="page-section pt-4">
        <div class="container">
            <nav aria-label="Breadcrumb">
                <ol class="breadcrumb bg-transparent mb-4">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    {{-- <li class="breadcrumb-item"><a href="{{ route('tintuc',['slug'=>$cate->slug]) }}"> {{ $cate->title }} </a></li> --}}
                    <li class="breadcrumb-item active" aria-current="page">{{ $projectDetail->title }}</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog-single-wrap">
                        <div class="post-thumbnail">
                            <img src="{{ asset($projectDetail->image) }}" class="img-fluid" alt="" width="100%">
                        </div>
                        <h1 class="post-title">
                            {{ $projectDetail->title }}
                        </h1>
                        <div class="post-meta">
                            <div class="post-author">
                                <span class="text-grey">By</span> <a href="#">Admin</a>
                            </div>
                            <span class="gap">|</span>
                            <div class="post-date">
                                <a href="#"> {{ $projectDetail->updated_at }} </a>
                            </div>
                            <span class="gap">|</span>
                            {{-- <div>
                                <a href="#">StreetStyle</a>, <a href="#">Fashion</a>, <a href="#">Couple</a>
                            </div> --}}
                            <span class="gap">|</span>
                            <div class="post-comment-count">
                                <a href="#">8 Comments</a>
                            </div>
                        </div>
                        <div class="post-content">
                            {!! $projectDetail->content !!}
                                {{-- <div class="post-tags">
                                    <p class="mb-2">Tags:</p>
                                    <a href="#" class="tag-link">LifeStyle</a>
                                    <a href="#" class="tag-link">Food</a>
                                    <a href="#" class="tag-link">Coronavirus</a>
                                </div> --}}
                        </div>
                    </div>
                    <!-- .blog-single-wrap -->

                    {{-- <div class="comment-form-wrap pt-5">
                        <h3 class="mb-5">Leave a comment</h3>
                        <form action="#" class="">
                            <div class="form-row form-group">
                                <div class="col-md-6">
                                    <label for="name">Name *</label>
                                    <input type="text" class="form-control" id="name">
                                </div>
                                <div class="col-md-6">
                                    <label for="email">Email *</label>
                                    <input type="email" class="form-control" id="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="website">Website</label>
                                <input type="url" class="form-control" id="website">
                            </div>

                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea name="msg" id="message" cols="30" rows="8" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Post Comment" class="btn btn-primary">
                            </div>

                        </form>
                    </div> --}}
                    <!-- .comment-form-wrap -->
                </div>

                <div class="col-lg-4">
                    <div class="widget">
                        

                        <div class="widget-box">
                            <h3 class="widget-title">Dự án mới</h3>
                            <div class="divider"></div>
                            @foreach($recentProject as $item)
                                <div class="blog-item">
                                    <div class="content">
                                        <h6 class="post-title"><a href="{{ route('duan.show',['slug'=>$item->slug]) }}">{{ $item->title }}</a></h6>
                                        <div class="meta">
                                            <a href="#"><span class="mai-calendar"></span> {{ $item->created_at }} </a>
                                            <a href="#"><span class="mai-person"></span> Admin</a>
                                            <a href="#"><span class="mai-chatbubbles"></span> 19</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>

                        <div class="widget-box">
                            <h3 class="widget-title">Lời cám ơn</h3>
                            <div class="divider"></div>
                            <p>
                                Chúng tôi thực sự biết ơn bạn vì đã chọn chúng tôi làm nhà cung cấp dịch vụ và cho chúng tôi cơ hội phát triển. Không có thành tựu nào của chúng tôi có thể đạt được nếu không có bạn và sự hỗ trợ vững chắc của bạn.
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- .container -->
    </div>
    <!-- .page-section -->
</main>




@endsection