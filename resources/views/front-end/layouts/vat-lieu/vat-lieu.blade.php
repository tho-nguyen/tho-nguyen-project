@extends('front-end.app')

@section('content')

@include('front-end.components.intro',['category'=>$category->title,'introName'=> 'Vật liệu xây dựng']);
    <main>
        <div class="page-section">
            <div class="container-fluid">
                <div class="row my-5">
                    <div class="col-md-12 col-lg-8 mt-3">
                        <div class="row">
                            @foreach($products as $prd)
                        
                        <div class="col-md-6 col-lg-4">
                            <div class="card-blog p-4">
                                <a href="{{ asset($prd->image)}}" data-fancybox="portfolio">
                                    <img src="{{ asset($prd->image)}}" width="100%" alt="">
                                </a>
                                <div class="d-flex flex-column justify-content-between">
                                    <div class="body mt-3">
                                        <div class="post-title">
                                            <a href="#!" class="">
                                                {{ $prd->title }}
                                            </a>
                                        </div>
                                        <div class="post-excerpt">
                                            {{ $prd->description }}
                                        </div>
                                    </div>
                                    <div class="mt-4 d-flex align-items-center justify-content-between">
                                        <p class="mb-0 fg-accent"> {{ number_format($prd->price) }} <sup>đ</sup> </p>
                                        <a href="#!">Liên hệ <span class="mai-chevron-forward text-sm"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        
                        </div>
                    </div>
                    <div class="col-lg-4">
                        @include('front-end.components.sidebar_right')
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
    </main>



@endsection