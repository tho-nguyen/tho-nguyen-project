@extends('front-end.app')

@section('content')

@include('front-end.components.intro',['category'=>'Về chúng tôi','introName'=> 'Giới thiệu']);


    <main>
        <div class="page-section pt-4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="blog-single-wrap">
                            <div class="post-thumbnail">
                                <img src="{{ asset($introduce->image) }}" class="img-fluid" alt="">
                            </div>
                            <div class="post-content">
                                {!! $introduce->content !!}
                            </div>
                        </div>
                        <!-- .blog-single-wrap -->
    
                        {{-- <div class="comment-form-wrap pt-5">
                            <h3 class="mb-5">Leave a comment</h3>
                            <form action="#" class="">
                                <div class="form-row form-group">
                                    <div class="col-md-6">
                                        <label for="name">Name *</label>
                                        <input type="text" class="form-control" id="name">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="email">Email *</label>
                                        <input type="email" class="form-control" id="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="website">Website</label>
                                    <input type="url" class="form-control" id="website">
                                </div>
    
                                <div class="form-group">
                                    <label for="message">Message</label>
                                    <textarea name="msg" id="message" cols="30" rows="8" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Post Comment" class="btn btn-primary">
                                </div>
    
                            </form>
                        </div> --}}
                    </div>
    
                    <div class="col-lg-4">
                        @include('front-end.components.sidebar_right')
                    </div>
    
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .page-section -->
</main>

@endsection
