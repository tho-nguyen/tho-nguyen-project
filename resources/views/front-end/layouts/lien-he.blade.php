@extends('front-end.app')

@section('content')

@include('front-end.components.intro',['category'=>'Liên hệ','introName'=> 'Liên hệ']);

<main>
    <div class="page-section">
        <div class="container">
            <div class="text-center">
                <h2 class="title-section mb-3">Liên hệ với chúng tôi</h2>
            </div>
            <div class="row justify-content-center mt-5">
                <div class="col-lg-8">
                    @include('front-end.components.form-contact',['route'=> route('contact.post')])
                </div>
            </div>
        </div>
        <!-- .container -->
    </div>
    <!-- .page-section -->

    <div class="maps-container">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d122445.89064291684!2d107.50707199761793!3d16.453543460636137!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141a115e1a7935f%3A0xbf3b50af70b5c7b7!2zVHAuIEh14bq_LCBUaOG7q2EgVGhpw6puIEh14bq_LCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1638004364150!5m2!1svi!2s" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>
</main>

@endsection