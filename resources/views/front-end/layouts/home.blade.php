@extends('front-end.app')

@section('content')

    <div class="page-banner home-banner mb-5">
        <div class="slider-wrapper">
            <div class="owl-carousel hero-carousel">
                @foreach($sliders as $slider)
                    <div class="hero-carousel-item">
                        <img src="{{ asset($slider->image) }}" alt="">
                    </div>
                @endforeach
            </div>
        </div>
        <!-- .slider-wrapper -->
    </div>
<!-- .page-banner -->
    <main>
        <div class="page-section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 py-3">
                        <div class="subhead">Về chúng tôi</div>
                        <h2 class="title-section fg-primary"> Công ty TNHH Xây dựng và Thương mại BKH  </h2>

                        <p>
                            Lấy uy tín chất lượng làm thước đo giá trị thương hiệu, HỮU TIỀN cam kết mang đến cho khách hàng sản phẩm là các công trình đạt tiêu chuẩn cao về kỹ – mỹ thuật với chi phí cạnh tranh; đồng thời góp phần làm thay đổi diện mạo xã hội thông qua những công trình này.                    
                        </p>

                        <p>
                            Minh bạch trong mọi hoạt động. Quản trị minh bạch, hợp tác minh bạch và lợi ích minh bạch.
                        </p>

                        <p>
                            Không ngừng sáng tạo, cải tiến toàn diện hệ thống quản lý chất lượng đáp ứng yêu cầu ngày càng cao của Khách hàng.
                        </p>

                        <a href="{{ url('gioi-thieu') }}" class="btn btn-primary mt-4">Xem thêm</a>
                    </div>
                    <div class="col-lg-6 py-3">
                        <div class="about-img">
                            <img src="{{ asset('front-end/assets/img/about.jpg ')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .page Ve Chung Toi -->

        <div class="page-section">
            <div class="container">
                <div class="text-center">
                    <div class="subhead">lĩnh vực hoạt động</div>
                    <h2 class="title-section">Các danh mục lĩnh vực hoạt động</h2>
                </div>

                <div class="row">
                    @foreach($categoriesLinhVuc as $cateLinhVuc)
                        <div class="col-md-6 col-lg-4 col-xl-4 py-3 mb-3">
                            <div class="work-item text-center">
                                <a href="{{ route('linhvuc',['slug'=> $cateLinhVuc->slug]) }}" class="fg-black">
                                    <div class="img-fluid mb-4">
                                    <img class="img-cover" src="{{ asset( $cateLinhVuc->avatar )}}" width="100%" height="250px" alt="">
                                </div>
                                <h5>{{ $cateLinhVuc ->title }}</h5>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .page Danh Muc Linh Vuc -->

        <div class="page-section">
            <div class="container">
                <div class="text-center">
                    <div class="subhead">Sản phẩm</div>
                    <h2 class="title-section">Sản phẩm mới nhất</h2>
                </div>

                <div class="owl-carousel team-carousel mt-5">
                    @foreach($productsVatLieu as $prdVatLieu)
                        
                    <div class="team-wrap">
                        <div class="team-profile">
                            <img src="{{ asset($prdVatLieu->image)}}" width="100%" height="200px" alt="">
                        </div>
                        <div class="team-content">
                            <h5 class="d-2-lines"> {{ $prdVatLieu->title }} </h5>
                            <div class="social-button d-flex justify-content-between align-items-center">
                                <div class="text-sm fg-accent"> {{ number_format($prdVatLieu->price) }} <sup>đ</sup></div>
                                <a href="{{ url('lien-he') }}" class="btn btn-outline-primary btn-sm">Liên hệ</a>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .page San Pham Vat Lieu -->

        <div class="page-section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 py-3">
                        <div class="subhead">Dự án</div>
                        <h2 class="title-section">Các dự án mới nhất của chúng tôi</h2>
                    </div>
                    <div class="col-md-6 py-3 text-md-right">
                        <a href="{{ url('du-an') }}" class="btn btn-outline-primary">Xem thêm <span class="mai-arrow-forward ml-2"></span></a>
                    </div>
                </div>

                <div class="row mt-3">
                    @foreach($Duan as $du_an)
                        
                    
                    <div class="col-lg-4 py-3">
                        <div class="portfolio new-item">
                            <a href="{{ route('duan.show',['slug'=>$du_an->slug]) }}" >
                                <img src="{{ asset($du_an->image)}}" width="100%" alt="">
                                <div class="new-content">
                                    <h5 class="d-2-lines"> {{ $du_an->title }} </h5>
                                    <p> {{ $du_an->description }} </p>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .page Du An -->

        <div class="page-section">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 py-3">
                        <div class="subhead">Bất động sản</div>
                        <h2 class="title-section">Bất động sản mới nhất của chúng tôi</h2>
                    </div>
                    <div class="col-md-6 py-3 text-md-right">
                        <a href="{{ route('bds') }}" class="btn btn-outline-primary">Xem thêm <span class="mai-arrow-forward ml-2"></span></a>
                    </div>
                </div>

                <div class="row mt-3">
                    @foreach($BatDongSan as $bds)
                        
                    <div class="col-lg-4 py-3">
                        <div class="new-item">
                            <a href="{{ route('bds.show',['slug'=>$bds->slug]) }}" >
                                <img src="{{ asset($bds->image )}}" width="100%" alt="">
                                <div class="new-content">
                                    <h5 class="d-2-lines"> {{ $bds->title }} </h5>
                                    <p> {{ $bds->description }}  </p>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .page Bat Dong San -->


        <div class="page-section">
            <div class="container">
                <div class="text-center">
                    <div class="subhead">Tin Tức</div>
                    <h2 class="title-section">Tin tức nổi bật</h2>
                </div>

                <div class="row my-5 card-blog-row">
                    @foreach($News as $new)
                        
                    <div class="col-md-6 col-lg-3">
                        <div class="card-blog">
                            <img src="{{ asset($new->image)}}" width="100%" alt="">
                            <div class="body mt-3">
                                <div class="post-title">
                                    <a class="d-2-lines" href="{{ route('tintuc.show',['slug'=> $new->slug]) }}">
                                        {{ $new->title }}
                                    </a>
                                </div>
                                <div class="post-excerpt d-3-lines">
                                    {{ $new->description }}
                                </div>
                            </div>
                            <div class="footer">
                                <a href="{{ route('tintuc.show',['slug'=> $new->slug]) }}">Xem thêm <span class="mai-chevron-forward text-sm"></span></a>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .page Tin Tuc Noi Bat -->

        <div class="page-section">
            <div class="container">
                <div class="text-center">
                    <h2 class="title-section mb-3">Liên hệ với chúng tôi</h2>
                </div>
                <div class="row justify-content-center mt-5">
                    <div class="col-lg-8">

                        @include('front-end.components.form-contact',['route'=> route('home.store')])
                    </div>
                </div>
            </div>
            <!-- .container -->
        </div>
        <!-- .page-section -->

        <div class="page-section">
            <div class="container-fluid">
                <div class="row row-cols-md-3 row-cols-lg-5 justify-content-center text-center">
                    <div class="py-3 px-5">
                        <img src="{{ asset('front-end/assets/img/clients/airbnb.png')}}" alt="">
                    </div>
                    <div class="py-3 px-5">
                        <img src="{{ asset('front-end/assets/img/clients/google.png')}}" alt="">
                    </div>
                    <div class="py-3 px-5">
                        <img src="{{ asset('front-end/assets/img/clients/mailchimp.png')}}" alt="">
                    </div>
                    <div class="py-3 px-5">
                        <img src="{{ asset('front-end/assets/img/clients/paypal.png')}}" alt="">
                    </div>
                    <div class="py-3 px-5">
                        <img src="{{ asset('front-end/assets/img/clients/stripe.png')}}" alt="">
                    </div>
                </div>
            </div>
            <!-- .container-fluid -->
        </div>
        <!-- .page-section -->

    </main>

@endsection
