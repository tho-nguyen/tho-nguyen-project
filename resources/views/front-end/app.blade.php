<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="copyright" content="MACode ID, https://macodeid.com/">

    <title>Công Ty Hữu Tiếns</title>

    <link rel="stylesheet" href="{{ asset('front-end/assets/css/bootstrap.css') }}">

    <link rel="stylesheet" href="{{ asset('front-end/assets/css/maicons.css') }}">

    <link rel="stylesheet" href="{{ asset('front-end/assets/vendor/animate/animate.css') }}">

    <link rel="stylesheet" href="{{ asset('front-end/assets/vendor/owl-carousel/css/owl.carousel.css') }}">

    <link rel="stylesheet" href="{{ asset('front-end/assets/vendor/fancybox/css/jquery.fancybox.css') }}">

    <link rel="stylesheet" href="{{ asset('front-end/assets/css/theme.css') }}">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />

</head>


<body>
    <div id="app">
        @include('front-end.components.header')

        @yield('content')

        @include('front-end.components.footer')

    </div>

    <script src="{{ asset('front-end/assets/js/jquery-3.5.1.min.js') }}"></script>

    <script src="{{ asset('front-end/assets/js/bootstrap.bundle.min.js') }}"></script>

    <script src="{{ asset('front-end/assets/vendor/owl-carousel/js/owl.carousel.min.js') }}"></script>

    <script src="{{ asset('front-end/assets/vendor/wow/wow.min.js') }}"></script>

    <script src="{{ asset('front-end/assets/vendor/fancybox/js/jquery.fancybox.min.js') }}"></script>

    <script src="{{ asset('front-end/assets/vendor/isotope/isotope.pkgd.min.js') }}"></script>

    <script src="{{ asset('front-end/assets/js/google-maps.js') }}"></script>

    <script src="{{ asset('front-end/assets/js/theme.js') }}"></script>

    
</body>
</html>
