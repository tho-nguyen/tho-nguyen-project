<?php
use Illuminate\Database\Seeder;

/**
 * Class AdminUserSeeder
 */
class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            factory(App\User::class)->create([
                    "name" => "admin",
                    "email" => "admin@gmail.com",
                    "username" => "admin",
                    "password" => bcrypt(env('ADMIN_PWD', 'admin'))]
            );
        } catch (\Illuminate\Database\QueryException $exception) {

        }
    }
}