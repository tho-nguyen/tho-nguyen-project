<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arPermissions = [
            "1" => ["HomeController@index", "Trang chủ"],

            "2" => ["UsersController@index", "Tài khoản người dùng"],
            "3" => ["UsersController@show", "Tài khoản người dùng"],
            "4" => ["UsersController@store", "Tài khoản người dùng"],
            "5" => ["UsersController@update", "Tài khoản người dùng"],
            "6" => ["UsersController@destroy", "Tài khoản người dùng"],
            "7" => ["UsersController@active", "Tài khoản người dùng"],
            //Trường hợp cho phép người dùng sửa, thì cho phép sửa profile của người dùng đó
            "8" => ["UsersController@postProfile", "Tài khoản người dùng"],

            "9" => ["RolesController@index", "Quản lý Vai trò"],
            "10" => ["RolesController@show", "Quản lý Vai trò"],
            "11" => ["RolesController@store", "Quản lý Vai trò"],
            "12" => ["RolesController@update", "Quản lý Vai trò"],
            "13" => ["RolesController@destroy", "Quản lý Vai trò"],
            "14" => ["RolesController@active", "Quản lý Vai trò"],

            "15" => ["SettingController", "Cấu hình công ty"],

            //Quyền module News
            "16" => ["CategoryController@index", "Danh mục tin tức"],
            "17" => ["CategoryController@show", "Danh mục tin tức"],
            "18" => ["CategoryController@store", "Danh mục tin tức"],
            "19" => ["CategoryController@update", "Danh mục tin tức"],
            "20" => ["CategoryController@destroy", "Danh mục tin tức"],
            "21" => ["CategoryController@active", "Danh mục tin tức"],

            "22" => ["NewsController@index", "Tin tức"],
            "23" => ["NewsController@show", "Tin tức"],
            "24" => ["NewsController@store", "Tin tức"],
            "25" => ["NewsController@update", "Tin tức"],
            "26" => ["NewsController@destroy", "Tin tức"],
            "27" => ["NewsController@active", "Tin tức"],

            //Quyền module Theme
            "28" => ["SysMenuController@index", "Hệ thống menu"],
            "29" => ["SysMenuController@show", "Hệ thống menu"],
            "30" => ["SysMenuController@store", "Hệ thống menu"],
            "31" => ["SysMenuController@update", "Hệ thống menu"],
            "32" => ["SysMenuController@destroy", "Hệ thống menu"],
            "33" => ["SysMenuController@active", "Hệ thống menu"],

            "34" => ["PageController@index", "Trang"],
            "35" => ["PageController@show", "Trang"],
            "36" => ["PageController@store", "Trang"],
            "37" => ["PageController@update", "Trang"],
            "38" => ["PageController@destroy", "Trang"],
            "39" => ["PageController@active", "Trang"],

            "40" => ["SliderController@index", "Slider"],
            "41" => ["SliderController@show", "Slider"],
            "42" => ["SliderController@store", "Slider"],
            "43" => ["SliderController@update", "Slider"],
            "44" => ["SliderController@destroy", "Slider"],
            "45" => ["SliderController@active", "Slider"],

            "46" => ["CustomerController@index", "Khách hàng"],
            "47" => ["CustomerController@show", "Khách hàng"],
            "48" => ["CustomerController@store", "Khách hàng"],
            "49" => ["CustomerController@update", "Khách hàng"],
            "50" => ["CustomerController@destroy", "Khách hàng"],
            "51" => ["CustomerController@active", "Khách hàng"],

            //Quyền module Hỗ trợ
            "52" => ["ContactController@index", "Liên hệ"],
            "53" => ["NewsletterController@index", "Đăng ký nhận tin"],

            //Quyền module Danh mục tin tức
            "54" => ["NewCategoryController@index", "Danh mục tin tức"],
            "55" => ["NewCategoryController@show", "Danh mục tin tức"],
            "56" => ["NewCategoryController@store", "Danh mục tin tức"],
            "57" => ["NewCategoryController@update", "Danh mục tin tức"],
            "58" => ["NewCategoryController@destroy", "Danh mục tin tức"],
            "59" => ["NewCategoryController@active", "Danh mục tin tức"],

            //Quyền module Sản phẩm
            "60" => ["LinhvucController@index", "Danh mục lĩnh vực"],
            "61" => ["LinhvucController@show", "Danh mục lĩnh vực"],
            "62" => ["LinhvucController@store", "Danh mục lĩnh vực"],
            "63" => ["LinhvucController@update", "Danh mục lĩnh vực"],
            "64" => ["LinhvucController@destroy", "Danh mục lĩnh vực"],
            "65" => ["LinhvucController@active", "Danh mục lĩnh vực"],

            //Quyền module Sản phẩm
            "66" => ["BaivietController@index", "Bài viết lĩnh vực"],
            "67" => ["BaivietController@show", "Bài viết lĩnh vực"],
            "68" => ["BaivietController@store", "Bài viết lĩnh vực"],
            "69" => ["BaivietController@update", "Bài viết lĩnh vực"],
            "70" => ["BaivietController@destroy", "Bài viết lĩnh vực"],
            "71" => ["BaivietController@active", "Bài viết lĩnh vực"],

             //Quyền module Sản phẩm
             "72" => ["DuanController@index", "Dự án"],
             "73" => ["DuanController@show", "Dự án"],
             "74" => ["DuanController@store", "Dự án"],
             "75" => ["DuanController@update", "Dự án"],
             "76" => ["DuanController@destroy", "Dự án"],
             "77" => ["DuanController@active", "Dự án"],


              //Quyền module Sản phẩm
              "78" => ["BatDongSanController@index", "Bất Động Sản"],
              "79" => ["BatDongSanController@show", "Bất Động Sản"],
              "80" => ["BatDongSanController@store", "Bất Động Sản"],
              "81" => ["BatDongSanController@update", "Bất Động Sản"],
              "82" => ["BatDongSanController@destroy", "Bất Động Sản"],
              "83" => ["BatDongSanController@active", "Bất Động Sản"],

                //Quyền module Sản phẩm
                "84" => ["VatlieuController@index", "Vật liệu xây dựng"],
                "85" => ["VatlieuController@show", "Vật liệu xây dựng"],
                "86" => ["VatlieuController@store", "Vật liệu xây dựng"],
                "87" => ["VatlieuController@update", "Vật liệu xây dựng"],
                "88" => ["VatlieuController@destroy", "Vật liệu xây dựng"],
                "89" => ["VatlieuController@active", "Vật liệu xây dựng"],
                    //Quyền module Sản phẩm
              "90" => ["SanPhamVatLieuController@index", "Sản phẩm vật liệu"],
              "91" => ["SanPhamVatLieuController@show", "Sản phẩm vật liệu"],
              "92" => ["SanPhamVatLieuController@store", "Sản phẩm vật liệu"],
              "93" => ["SanPhamVatLieuController@update", "Sản phẩm vật liệu"],
              "94" => ["SanPhamVatLieuController@destroy", "Sản phẩm vật liệu"],
              "95" => ["SanPhamVatLieuController@active", "Sản phẩm vật liệu"],

            //Quyền module Sản phẩm
            "96" => ["GioithieuController@index", "Giới thiệu"],
            "97" => ["GioithieuController@show", "Giới thiệu"],
            "98" => ["GioithieuController@store", "Giới thiệu"],
            "99" => ["GioithieuController@update", "Giới thiệu"],
            "100" => ["GioithieuController@destroy", "Giới thiệu"],
            "101" => ["GioithieuController@active", "Giới thiệu"],

            //Quyền module Sản phẩm
            "102" => ["YeuCauController@index", "Yêu cầu tư vấn"],
            "103" => ["YeuCauController@show", "Yêu cầu tư vấn"],
            "104" => ["YeuCauController@store", "Yêu cầu tư vấn"],
            "105" => ["YeuCauController@update", "Yêu cầu tư vấn"],
            "106" => ["YeuCauController@destroy", "Yêu cầu tư vấn"],
            "107" => ["YeuCauController@active", "Yêu cầu tư vấn"],
        ];

        //ADD PERMISSIONS - Thêm các quyền
        DB::table('permissions')->delete(); //empty permission
        $addPermissions = [];
        foreach ($arPermissions as $name => $label) {
            $addPermissions[] = [
                'id' => $name,
                'name' => $label[0],
                'label' => $label[1],
            ];
        }
        DB::table('permissions')->insert($addPermissions);

        //ADD ROLE - Them vai tro
        //        DB::table( 'roles' )->delete();//empty permission
        $datenow = date('Y-m-d H:i:s');
        $role = [
            ['id' => 1, 'name' => 'company_admin', 'label' => 'Admin', 'created_at' => $datenow, 'updated_at' => $datenow],
            ['id' => 2, 'name' => 'project_manager', 'label' => 'Project manager', 'created_at' => $datenow, 'updated_at' => $datenow],
            ['id' => 3, 'name' => 'account_payable', 'label' => 'Account payable', 'created_at' => $datenow, 'updated_at' => $datenow],
            ['id' => 4, 'name' => 'user', 'label' => 'User', 'created_at' => $datenow, 'updated_at' => $datenow],
            ['id' => 5, 'name' => 'customer', 'label' => 'Customer', 'created_at' => $datenow, 'updated_at' => $datenow],
        ];
        $addRoles = [];
        foreach ($role as $key => $label) {
            if (\App\Role::where('id', $label['id'])->count() == 0) {
                $addRoles[] = [
                    'id' => $label['id'],
                    'name' => $label['name'],
                    'label' => $label['label'],
                    'created_at' => $datenow,
                    'updated_at' => $datenow,
                ];
            }
        }
        //KIỂM TRA VÀ THÊM CÁC VAI TRÒ TRUYỀN VÀO NẾU CÓ
        if (count($addRoles) > 0) {
            DB::table('roles')->insert($addRoles);
        }

        //BỔ SUNG ID QUYỀN NẾU CÓ
        //Full quyền Admin công ty
        $persAdmin = \App\Permission::pluck('id');

        //Quyền quản lí task
        $persManager = [
            1, 5, 8,
        ];

        //Quyền thanh toán
        $persPay = [
            1, 5, 8,
        ];

        //Quyền cộng tác viên (vendor)
        $persVendor = [
            1, 5, 8,
        ];

        //Quyền khách hàng
        $persCustomer = [
            1, 5, 8,
        ];

        //Gán quyền vào Vai trò Admin
        $rolePerAdminCompany = \App\Role::findOrFail(1);
        $rolePerAdminCompany->permissions()->sync($persAdmin);

        //Gán quyền vào Vai trò Project Manager
        $rolePerCompanyEmployee = \App\Role::findOrFail(2);
        $rolePerCompanyEmployee->permissions()->sync($persManager);

        //Gán quyền vào Vai trò Account Pay
        $rolePerAgentAdmin = \App\Role::findOrFail(3);
        $rolePerAgentAdmin->permissions()->sync($persPay);

        //Gán quyền vào Vai trò Vendor
        $rolePerAgentEmployee = \App\Role::findOrFail(4);
        $rolePerAgentEmployee->permissions()->sync($persVendor);

        //Gán quyền vào Vai trò Customer
        $rolePerCustomer = \App\Role::findOrFail(5);
        $rolePerCustomer->permissions()->sync($persCustomer);

        //Set tài khoản ID=1 là Admin
        $roleAdmin = \App\User::findOrFail(1);
        $roleAdmin->roles()->sync([1]);
    }
}
