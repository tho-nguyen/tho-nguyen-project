<?php
/**
 * Setting app
 */
return [
    "app_logo" => "<b>Admin</b>",
    "app_logo_mini" => "<b>Admin</b>",

	"avatar_default" => '/images/avatar.png',

    "active" => 1,
    "inactive" => 0,

    "public_avatar" => '/images/avatar/',

    "perpage" => 20,

    'paginate' => [
        'page5' => 5,
        'page6' => 6,
        'page8' => 8,
        'page12' => 12,
    ],

	"log_active" => true,
    "format" => [
	    "datetime" => "d/m/Y H:i",
	    "date" => "d/m/Y",
	    "date_js" => 'dd/mm/yyyy',
    ],
    "approved" => [
	    'cancel' => -10,//hủy vé
	    'tmp' => 5,//lưu tạm
	    'save' => 10//lưu
    ],
	
	//set roles list
	"roles" => [
		'company_admin' => 'company_admin',
		'company_employee' => 'company_employee',
		'company_booking' => 'company_booking',
		'agent_admin' => 'agent_admin',
		'agent_employee' => 'agent_employee',
        'customer' => 'customer'
	],

    'settings_site' => [
        [
            "key" => "company_logo",
            "value" => "",
            "description" => "Đường dẫn lưu logo",
            "type"=>"image",
            "group_data"=>"Thông tin công ty", // Thông tin công ty
        ],
        [
            "key" => "company_website",
            "value" => "",
            "description" =>"Tên website",
            "type"=>"text",
            "group_data"=>"Thông tin công ty",// Thông tin công ty
        ],
        [
            "key" => "company_address",
            "value" => "",
            "description" =>"Địa chỉ",
            "group_data"=>"Thông tin công ty",// Thông tin công ty
            "type"=>"text"
        ],
        [
            "key" => "company_phone",
            "value" => "",
            "description" =>"Số điện thoại",
            "group_data"=>"Thông tin công ty",// Thông tin công ty
            "type"=>"text"
        ],
        [
            "key" => "company_hotline",
            "value" => "",
            "description" =>"Hotline",
            "group_data"=>"Thông tin công ty",// Thông tin công ty
            "type"=>"text"
        ],
        [
            "key" => "company_email",
            "value" => "",
            "description" =>"Email",
            "group_data"=>"Thông tin công ty",// Thông tin công ty
            "type"=>"text"
        ],
        [
            "key" => "company_link",
            "value" => '',
            "description" =>"Website",
            "group_data"=>"Thông tin công ty",// Thông tin công ty
            "type"=>"text"
        ],
        [
            "key" => "ministry_industry_trade_link",
            "value" => '',
            "description" =>"Địa chỉ 'Đã thông báo bộ công thương'",
            "group_data"=>"Thông tin công ty",// Thông tin công ty
            "type"=>"text"
        ]
    ],
]
?>