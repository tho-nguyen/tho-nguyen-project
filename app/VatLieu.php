<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class VatLieu extends Model
{
    use Sortable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "vatlieu";

    protected $sortable = [
        'title',
        'updated_at'
    ];

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title','slug','avatar','description','active','parent_id','keywords'];


    public function parent(){
        return $this->belongsTo('App\Vatlieu','parent_id');
    }

    public function sanpham(){
        return $this->hasMany('App\SanPhamVatLieu','vatlieu_id');
    }
    public static function getListVatlieuToArray($vatlieu, $parent_id = '', $level = '', $result = []){
        global $result;
        foreach ( $vatlieu as $key => $item){
            if ($item['parent_id'] == $parent_id){
                $result[$item->id] = $level. $item['title'];
                unset($vatlieu[$key]);
                self::getListVatlieuToArray($vatlieu, $item['id'], $level.'-- ', $result);
            }
        }
        return $result;
    }


    static public function uploadAndResize($image, $width = 450, $height = null){
        if(empty($image)) return;
        $folder = "/images/vatlieu/";
        if(!\Storage::disk(config('filesystems.disks.public.visibility'))->has($folder)){
            \Storage::makeDirectory(config('filesystems.disks.public.visibility').$folder);
        }
        //getting timestamp
        $timestamp = Carbon::now()->toDateTimeString();
        $fileExt = $image->getClientOriginalExtension();
        $filename = str_slug(basename($image->getClientOriginalName(), '.'.$fileExt));
        $pathImage = str_replace([' ', ':'], '-', $folder.$timestamp. '-' .$filename.'.'.$fileExt);

        $img = \Image::make($image->getRealPath())->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });

        $img->save(storage_path('app/public').$pathImage);

        return config('filesystems.disks.public.path').$pathImage;
    }



    public static function getVatlieu($treeCategories){
        $vatlieu = self::getListVatlieuToArray($treeCategories);
        $vatlieu = Arr::prepend(!empty($vatlieu) ? $vatlieu : [], __('Vui lòng chọn'), '');
        return $vatlieu;
    }
}
