<?php

namespace App\Http\Controllers;


use App\News;
use App\SysMenu;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    /**
     * Gọi ajax: sẽ gọi đến hàm = tên $action
     * @param Request $action
     * @param Request $request
     * @return mixed
     */
    public function index($action, Request $request)
    {
        return $this->{$action}($request);
    }



    // newss
    public function activeNews(Request $request)
    {
        $ids = $request->ids;
        $arrId = explode(',', $ids, -1);
        foreach ($arrId as $item) {
            $news = News::findOrFail($item);
            $active = $news->active == config('settings.active') ? config('settings.inactive') : config('settings.active');
            \DB::table('news')->where('id', $news->id)->update(['active' => $active]);
        }
        toastr()->success("Kích hoạt thành công!");

        return \response()->json(['success' => 'ok']);
    }

    public function deleteNews(Request $request)
    {
        $ids = $request->ids;
        $arrId = explode(',', $ids, -1);
        foreach ($arrId as $item) {
            News::destroy($item);
        }
        toastr()->success('Xóa thành công');

        return \response()->json(['success' => 'ok']);
    }

}
