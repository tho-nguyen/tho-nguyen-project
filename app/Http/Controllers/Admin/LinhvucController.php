<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Danhmuclinhvuc;
use App\SysMenu;
use App\Traits\Authorizable;
use Illuminate\Support\Str;

class LinhvucController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = config('settings.perpage');
        $linhvuc = new Danhmuclinhvuc();

        if (!empty($keyword)) {
            $linhvuc = $linhvuc->where('title','LIKE', "%$keyword%");
        }
        $linhvuc = $linhvuc->sortable()->paginate($perPage);

        return view('admin.linhvuc.index', compact('linhvuc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $linhvuc = Danhmuclinhvuc::getLinhvuc(Danhmuclinhvuc::all());
        return view('admin.linhvuc.create', compact('linhvuc'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        if (empty($request->get('slug'))) {
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        
        $requestData['slug'] = Str::slug($requestData['title']);


        \DB::transaction(function () use ($request, $requestData){
            if (!empty($request->hasFile('avatar'))) {
                $requestData['avatar'] = Danhmuclinhvuc::uploadAndResize($request->file('avatar'));
            }else {
                $requestData['avatar'] = null;
            }
            Danhmuclinhvuc::create($requestData);
        });

        toastr()->success(__('Thêm thành công'));

        return redirect('admin/linhvuc');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $linhvuc1 = Danhmuclinhvuc::findOrFail($id);
        $linhvuc = Danhmuclinhvuc::where('id', $linhvuc1->parent_id)->first();
        return view('admin.linhvuc.show', compact('linhvuc1','linhvuc'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $linhvuc = Danhmuclinhvuc::getLinhvuc(Danhmuclinhvuc::all()); 
        $linhvuc1 = Danhmuclinhvuc::findOrFail($id);
        return view('admin.linhvuc.edit', compact('linhvuc1', 'linhvuc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $linhvuc1 = Danhmuclinhvuc::findOrFail($id);
        $requestData = $request->all();
        if (empty($request->get('slug'))) {
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        
        $requestData['slug'] = Str::slug($requestData['title']);

        if (empty($requestData['active'])){
            $requestData['active'] = config('settings.inactive');
        }
        \DB::transaction(function () use ($request, $requestData, $linhvuc1){
            if($request->hasFile('avatar')) {
                \File::delete($linhvuc1->image);
                $requestData['avatar'] = Danhmuclinhvuc::uploadAndResize($request->file('avatar'));
            }
            $linhvuc1->update($requestData);
        });

        toastr()->success(__('Sủa thành công'));
        return redirect('admin/linhvuc');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Danhmuclinhvuc::destroy($id);

        toastr()->success(__('Xóa thành công'));

        return redirect('admin/linhvuc');
    }
}
