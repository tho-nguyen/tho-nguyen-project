<?php

namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SysMenu;
use Illuminate\Support\Str;
use App\VatLieu;
use App\SanPhamVatLieu;
class SanPhamVatLieuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sanpham_cate = VatLieu::getVatlieu(VatLieu::all());
        $perPage = config('settings.perpage');
 
        $sanpham = new SanPhamVatLieu();
        $active = SanPhamVatLieu::getListActive();

        if(!empty($request->get('vatlieu_id'))){
            $sanpham = $sanpham->where('vatlieu_id', $request->get('vatlieu_id'));
        }

        $sanpham = $sanpham->with('vatlieu');

        $sanpham = $sanpham->sortable(['updated_at' => 'desc'])->paginate($perPage);
        return view('admin.sanpham.index', compact('sanpham', 'active','sanpham_cate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sanpham_cate = VatLieu::getVatlieu(VatLieu::all());
        // dd($news_cate);
        return view('admin.sanpham.create', compact('sanpham_cate'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        if (empty($request->get('slug'))) {
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        
        $requestData['slug'] = Str::slug($requestData['title']);

        \DB::transaction(function () use ($request, $requestData) {
            if ($request->hasFile('image')) {
                $requestData['image'] = SanPhamVatLieu::uploadAndResize($request->file('image'));
            }
            SanPhamVatLieu::create($requestData);
        });


        toastr()->success(__('Thêm thành công'));

        return redirect('admin/sanpham');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $sanpham = SanPhamVatLieu::findOrFail($id);
        //Lấy đường dẫn cũ
        $backUrl = $request->get('back_url');
        return view('admin.sanpham.show', compact('sanpham','backUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $sanpham = SanPhamVatLieu::findOrFail($id);
        $sanpham_cate = VatLieu::getVatlieu(VatLieu::all());
        $backUrl = $request->get('back_url');
        return view('admin.sanpham.edit', compact('sanpham', 'sanpham_cate', 'backUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sanpham = SanPhamVatLieu::findOrFail($id);
        $requestData = $request->all();
        if (empty($request->get('slug'))) {
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        
        $requestData['slug'] = Str::slug($requestData['title']);


        if (empty($requestData['active'])) {
            $requestData['active'] = 0;
        }

        \DB::transaction(function () use ($request, $requestData, $sanpham) {
            if ($request->hasFile('image')) {
                \File::delete($sanpham->image);
                $requestData['image'] = SanPhamVatLieu::uploadAndResize($request->file('image'));
            }
            $sanpham->update($requestData);
        });

        toastr()->success(__('Cập nhật thành công'));

        if ($request->has('back_url')) {
            $backUrl = $request->get('back_url');
            if (!empty($backUrl)) {
                return redirect($backUrl);
            }
        }

        return redirect('admin/sanpham');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sanpham = SanPhamVatLieu::findOrFail($id);
        if (!empty($sanpham->image)) {
            \File::delete($sanpham->image);
        }
        SanPhamVatLieu::destroy($id);
        toastr()->success(__('Xóa thành công'));

        return redirect('admin/sanpham');
    }
}
