<?php

namespace App\Http\Controllers\Admin;

use App\NewCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\Newsletter;
use App\SysMenu;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Category;
use App\Setting;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category = Category::getCategories(Category::all());
        $perPage = config('settings.perpage');
        $news = new News();
        $active = News::getListActive();
        if(!empty($request->get('category_id'))){
            $news = $news->where('category_id', $request->get('category_id'));
        }

        $news = $news->with('category');

        $news = $news->sortable(['updated_at' => 'desc'])->paginate($perPage);
        return view('admin.news.index', compact('news', 'active','category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $news_cate = Category::getCategories(Category::all());
        // dd($news_cate);
        return view('admin.news.create', compact('news_cate'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        if (empty($request->get('slug'))) {
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        
        $requestData['slug'] = Str::slug($requestData['title']);

        \DB::transaction(function () use ($request, $requestData) {
            if ($request->hasFile('image')) {
                $requestData['image'] = News::uploadAndResize($request->file('image'));
            }
            News::create($requestData);
        });


        toastr()->success(__('Thêm thành công'));

        return redirect('admin/news');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $news = News::findOrFail($id);
        $backUrl = $request->get('back_url');
        return view('admin.news.show', compact('news','backUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $news = News::findOrFail($id);
        $news_cate = Category::getCategories(Category::all());
        $backUrl = $request->get('back_url');
        return view('admin.news.edit', compact('news', 'news_cate', 'backUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news = News::findOrFail($id);
        $requestData = $request->all();
        if (empty($request->get('slug'))) {
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        
        $requestData['slug'] = Str::slug($requestData['title']);

        if (empty($requestData['active'])) {
            $requestData['active'] = 0;
        }

        if (empty($requestData['is_focus'])) {
            $requestData['is_focus'] = 0;
        }

        \DB::transaction(function () use ($request, $requestData, $news) {
            if ($request->hasFile('image')) {
                \File::delete($news->image);
                $requestData['image'] = News::uploadAndResize($request->file('image'));
            }
            $news->update($requestData);
        });

        toastr()->success(__('Sửa thành công'));

        if ($request->has('back_url')) {
            $backUrl = $request->get('back_url');
            if (!empty($backUrl)) {
                return redirect($backUrl);
            }
        }

        return redirect('admin/news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $new = News::findOrFail($id);
        if (!empty($new->image)) {
            \File::delete($new->image);
        }
        News::destroy($id);
        toastr()->success(__('Xóa thành công'));

        return redirect('admin/news');
    }

   
}
