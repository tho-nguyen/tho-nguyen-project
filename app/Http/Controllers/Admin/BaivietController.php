<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Baivietlinhvuc;
use App\Danhmuclinhvuc;
use App\SysMenu;

class BaivietController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $linhvuc = Danhmuclinhvuc::getLinhvuc(Danhmuclinhvuc::all());
        // dd($linhvuc);
        $keyword = $request->get('search');
        $perPage = config('settings.perpage');
 
        $baiviet = new Baivietlinhvuc();
        $active = Baivietlinhvuc::getListActive();

        if (!empty($keyword)) {
            $news = $baiviet->where('title', 'LIKE', "%$keyword%");
        }
        if(!empty($request->get('danhmuc_id'))){
            $baiviet = $baiviet->where('danhmuc_id', $request->get('danhmuc_id'));
        }

        $baiviet = $baiviet->with('linhvuc');

        $baiviet = $baiviet->sortable(['updated_at' => 'desc'])->paginate($perPage);
        return view('admin.baiviet.index', compact('baiviet', 'active','linhvuc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $baiviet_danhmuc = Danhmuclinhvuc::getLinhvuc(Danhmuclinhvuc::all());
        // dd($baiviet_danhmuc);
        return view('admin.baiviet.create', compact('baiviet_danhmuc'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        if (empty($request->get('slug'))) {
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        
        $requestData['slug'] = Str::slug($requestData['title']);

        \DB::transaction(function () use ($request, $requestData) {
            if ($request->hasFile('image')) {
                $requestData['image'] = Baivietlinhvuc::uploadAndResize($request->file('image'));
            }
            Baivietlinhvuc::create($requestData);
        });


        toastr()->success(__('Bài viết đã được lưu'));

        return redirect('admin/baiviet');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $baiviet = Baivietlinhvuc::findOrFail($id);
        $backUrl = $request->get('back_url');
        return view('admin.baiviet.show', compact('baiviet','backUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $baiviet = Baivietlinhvuc::findOrFail($id);
        $baiviet_danhmuc = Danhmuclinhvuc::getLinhvuc(Danhmuclinhvuc::all());
        $backUrl = $request->get('back_url');
        return view('admin.baiviet.edit', compact('baiviet', 'baiviet_danhmuc', 'backUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $baiviet = Baivietlinhvuc::findOrFail($id);
        $requestData = $request->all();
        if (empty($request->get('slug'))) {
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        
        $requestData['slug'] = Str::slug($requestData['title']);

        if (empty($requestData['active'])) {
            $requestData['active'] = 0;
        }



        \DB::transaction(function () use ($request, $requestData, $baiviet) {
            if ($request->hasFile('image')) {
                \File::delete($baiviet->image);
                $requestData['image'] = Baivietlinhvuc::uploadAndResize($request->file('image'));
            }
            $baiviet->update($requestData);
        });

        toastr()->success(__('Bài viết đã cập nhật thành công'));

        if ($request->has('back_url')) {
            $backUrl = $request->get('back_url');
            if (!empty($backUrl)) {
                return redirect($backUrl);
            }
        }

        return redirect('admin/baiviet');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Baivietlinhvuc::destroy($id);

        toastr()->success(__('Bài viết đã xóa'));

        return redirect('admin/baiviet');
    }
}
