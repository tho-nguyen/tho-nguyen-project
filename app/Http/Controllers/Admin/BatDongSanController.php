<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\SysMenu;
use App\BatDongSan;

class BatDongSanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = config('settings.perpage');
 
        $batdongsan = new BatDongSan();
        $active = BatDongSan::getListActive();

        $batdongsan = $batdongsan->sortable(['updated_at' => 'desc'])->paginate($perPage);
        return view('admin.batdongsan.index', compact('batdongsan', 'active'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $batdongsan = BatDongSan::all();
        return view('admin.batdongsan.create', compact('batdongsan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
        $requestData = $request->all();
        if (empty($request->get('slug'))) {
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        
        $requestData['slug'] = Str::slug($requestData['title']);

        \DB::transaction(function () use ($request, $requestData) {
            if ($request->hasFile('image')) {
                $requestData['image'] = BatDongSan::uploadAndResize($request->file('image'));
            }
            BatDongSan::create($requestData);
        });


        toastr()->success(__('Thêm thành công'));

        return redirect('admin/batdongsan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $batdongsan = BatDongSan::findOrFail($id);
        //Lấy đường dẫn cũ
        $backUrl = $request->get('back_url');
        return view('admin.batdongsan.show', compact('batdongsan','backUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {

        $batdongsan = BatDongSan::findOrFail($id);
        $backUrl = $request->get('back_url');
        return view('admin.batdongsan.edit', compact('batdongsan', 'backUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required',
            'description' => 'required',
            'content' => 'required',


        ],
        [
            'title.required' => 'Vui lòng nhập tiêu đề!',
            'image.required' => 'Vui lòng đăng ảnh !',
            'description.required' => 'Vui lòng nhập mô tả !',
            'content.required' => 'Vui lòng nhập nội dung !',

        ]);
        $batdongsan = BatDongSan::findOrFail($id);
        $requestData = $request->all();
        if (empty($request->get('slug'))) {
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        
        $requestData['slug'] = Str::slug($requestData['title']);

        if (empty($requestData['active'])) {
            $requestData['active'] = 0;
        }



        \DB::transaction(function () use ($request, $requestData, $batdongsan) {
            if ($request->hasFile('image')) {
                \File::delete($batdongsan->image);
                $requestData['image'] = BatDongSan::uploadAndResize($request->file('image'));
            }
            $batdongsan->update($requestData);
        });

        toastr()->success(__('Sửa thành công'));

        if ($request->has('back_url')) {
            $backUrl = $request->get('back_url');
            if (!empty($backUrl)) {
                return redirect($backUrl);
            }
        }

        return redirect('admin/batdongsan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BatDongSan::destroy($id);

        toastr()->success(__('Xóa thành công'));

        return redirect('admin/batdongsan');
    }
    
}
