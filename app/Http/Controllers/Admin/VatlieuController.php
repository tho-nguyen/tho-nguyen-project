<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SysMenu;
use App\Traits\Authorizable;
use Illuminate\Support\Str;
use App\VatLieu;

class VatlieuController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource. 
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = config('settings.perpage');
        $vatlieu = new VatLieu();


        $vatlieu = $vatlieu->sortable()->paginate($perPage);

        return view('admin.vatlieu.index', compact('vatlieu'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vatlieu = VatLieu::getVatlieu(VatLieu::all());
        return view('admin.vatlieu.create', compact('vatlieu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        if (empty($request->get('slug'))) {
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        
        $requestData['slug'] = Str::slug($requestData['title']);


        \DB::transaction(function () use ($request, $requestData){
            if (!empty($request->hasFile('avatar'))) {
                $requestData['avatar'] = VatLieu::uploadAndResize($request->file('avatar'));
            }else {
                $requestData['avatar'] = null;
            }
            VatLieu::create($requestData);
        });

        toastr()->success(__('Thêm thành công'));

        return redirect('admin/vatlieu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vatlieu_cate = VatLieu::findOrFail($id);
        $vatlieu = VatLieu::where('id', $vatlieu_cate->parent_id)->first();
        return view('admin.vatlieu.show', compact('vatlieu','vatlieu_cate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vatlieu = VatLieu::getVatlieu(VatLieu::all()); 
        $vatlieu_cate = VatLieu::findOrFail($id);
        return view('admin.vatlieu.edit', compact('vatlieu_cate', 'vatlieu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vatlieu_cate = VatLieu::findOrFail($id);
        $requestData = $request->all();
        if (empty($request->get('slug'))) {
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        
        $requestData['slug'] = Str::slug($requestData['title']);

        if (empty($requestData['active'])){
            $requestData['active'] = config('settings.inactive');
        }
        \DB::transaction(function () use ($request, $requestData, $vatlieu_cate){
            if($request->hasFile('avatar')) {
                \File::delete($vatlieu_cate->image);
                $requestData['avatar'] = VatLieu::uploadAndResize($request->file('avatar'));
            }
            $vatlieu_cate->update($requestData);
        });

        toastr()->success(__('Cập nhật thành công'));
        return redirect('admin/vatlieu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        VatLieu::destroy($id);

        toastr()->success(__('Xóa thành công'));

        return redirect('admin/vatlieu');
    }
}
