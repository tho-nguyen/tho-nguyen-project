<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\SysMenu;
use App\GioiThieu;
class GioithieuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = config('settings.perpage');
        $gioithieu = new GioiThieu();
        $gioithieu = $gioithieu->sortable(['updated_at' => 'desc'])->paginate($perPage);
        return view('admin.gioithieu.index', compact('gioithieu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gioithieu = GioiThieu::all();
        return view('admin.gioithieu.create', compact('gioithieu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        \DB::transaction(function () use ($request, $requestData) {
            if ($request->hasFile('image')) {
                $requestData['image'] = GioiThieu::uploadAndResize($request->file('image'));
            }
            GioiThieu::create($requestData);
        });


        toastr()->success(__('Thêm thành công'));

        return redirect('admin/gioithieu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $gioithieu = GioiThieu::findOrFail($id);
        //Lấy đường dẫn cũ
        return view('admin.gioithieu.show', compact('gioithieu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $gioithieu = GioiThieu::findOrFail($id);
        return view('admin.gioithieu.edit', compact('gioithieu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gioithieu = GioiThieu::findOrFail($id);
        $requestData = $request->all();
        \DB::transaction(function () use ($request, $requestData, $gioithieu) {
            if ($request->hasFile('image')) {
                \File::delete($gioithieu->image);
                $requestData['image'] = GioiThieu::uploadAndResize($request->file('image'));
            }
            $gioithieu->update($requestData);
        });

        toastr()->success(__('Sửa thành công'));

        return redirect('admin/gioithieu/1/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        GioiThieu::destroy($id);

        toastr()->success(__('Xóa thành công'));

        return redirect('admin/gioithieu');
    }
}
