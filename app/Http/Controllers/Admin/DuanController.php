<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\SysMenu;
use App\Duan;


class DuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = config('settings.perpage');
 
        $duan = new Duan();
        $active = Duan::getListActive();

        $duan = $duan->sortable(['updated_at' => 'desc'])->paginate($perPage);
        return view('admin.du-an.index', compact('duan', 'active'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $duan = Duan::all();
        // dd($duan);
        return view('admin.du-an.create', compact('duan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();
        if (empty($request->get('slug'))) {
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        
        $requestData['slug'] = Str::slug($requestData['title']);

        \DB::transaction(function () use ($request, $requestData) {
            if ($request->hasFile('image')) {
                $requestData['image'] = Duan::uploadAndResize($request->file('image'));
            }
            Duan::create($requestData);
        });


        toastr()->success(__('Thêm thành công'));

        return redirect('admin/du-an');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $duan = Duan::findOrFail($id);
        $backUrl = $request->get('back_url');
        return view('admin.du-an.show', compact('duan','backUrl'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $duan = Duan::findOrFail($id);
        $backUrl = $request->get('back_url');
        return view('admin.du-an.edit', compact('duan', 'backUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $duan = Duan::findOrFail($id);
        $requestData = $request->all();
        if (empty($request->get('slug'))) {
            $requestData['slug'] = Str::slug($requestData['title']);
        }
        
        $requestData['slug'] = Str::slug($requestData['title']);


        if (empty($requestData['active'])) {
            $requestData['active'] = 0;
        }



        \DB::transaction(function () use ($request, $requestData, $duan) {
            if ($request->hasFile('image')) {
                \File::delete($duan->image);
                $requestData['image'] = Duan::uploadAndResize($request->file('image'));
            }
            $duan->update($requestData);
        });

        toastr()->success(__('Sửa thành công'));

        if ($request->has('back_url')) {
            $backUrl = $request->get('back_url');
            if (!empty($backUrl)) {
                return redirect($backUrl);
            }
        }

        return redirect('admin/du-an');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Duan::destroy($id);

        toastr()->success(__('Xóa thành công'));

        return redirect('admin/du-an');
    }
}
