<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
class Yeucau extends Model
{
    use Sortable;

    protected $table = 'yeucau';
    protected $primaryKey = 'id';
    protected $sortable = [

        'updated_at'
    ];
    protected $fillable = ['name','phone','description'];
}
