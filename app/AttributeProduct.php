<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class AttributeProduct extends Model
{
    use Sortable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_attributes';

    public $sortable = [
        'type',
        'name',
        'product_id',
        'updated_at',
    ];

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['key', 'value', 'active'];

   
    public function product()
    {
        return $this->belongsToMany('App\Product', 'product_id');
    }

    public static function getListAttrs()
    {
        $arr = ['color' => 'Màu sắc', 'size' => 'Kích cỡ', 'material' => 'Chất liệu', 'weight' => 'Cân nặng'];
        return $arr;
    }
}
