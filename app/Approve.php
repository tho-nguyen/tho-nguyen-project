<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Approve extends Model
{
    use Sortable;

    protected $table = 'approves';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public $sortable = [
        'id',
        'name',
        'number',
        'color',
        'updated_at'
    ];

    protected $fillable = ['name', 'number', 'color'];

    public function bookings(){
        return $this->hasMany('App\Booking', 'approve_id');
    }

}
