<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Notifications\Notifiable;
class Contact extends Model
{
    use Sortable,Notifiable;

    protected $table = 'contacts';
    protected $primaryKey = 'id';
    protected $sortable = [
        'fullname',
        'email',
        'updated_at'
    ];
    protected $fillable = ['fullname','email','address','phone','message','id','title'];
}
