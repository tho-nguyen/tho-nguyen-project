<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;

class Danhmuclinhvuc extends Model
{
    use Sortable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = "linhvuc";

    protected $sortable = [
        'title',
        'updated_at'
    ];

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id','title','slug','avatar','description','active','parent_id'];

    public function parent1(){
        return $this->belongsTo('App\Danhmuclinhvuc','parent_id');
    }

    public function baiviet(){
        return $this->hasMany('App\Baivietlinhvuc','danhmuc_id');
    }

    public static function getListLinhvucToArray($linhvuc, $parent_id = '', $level = '', $result = []){
        global $result;
        foreach ( $linhvuc as $key => $item){
            if ($item['parent_id'] == $parent_id){
                $result[$item->id] = $level. $item['title'];
                unset($linhvuc[$key]);
                self::getListLinhvucToArray($linhvuc, $item['id'], $level.'-- ', $result);
            }
        }
        return $result;
    }

    public static function getLinhvuc($treeLinhvuc){
        $linhvuc = self::getListLinhvucToArray($treeLinhvuc);
        $linhvuc = Arr::prepend(!empty($linhvuc) ? $linhvuc : [], __('Vui lòng chọn'), '');
        return $linhvuc;
    }


    static public function uploadAndResize($image, $width = 450, $height = null){
        if(empty($image)) return;
        $folder = "/images/categories/";
        if(!\Storage::disk(config('filesystems.disks.public.visibility'))->has($folder)){
            \Storage::makeDirectory(config('filesystems.disks.public.visibility').$folder);
        }
        //getting timestamp
        $timestamp = Carbon::now()->toDateTimeString();
        $fileExt = $image->getClientOriginalExtension();
        $filename = str_slug(basename($image->getClientOriginalName(), '.'.$fileExt));
        $pathImage = str_replace([' ', ':'], '-', $folder.$timestamp. '-' .$filename.'.'.$fileExt);

        $img = \Image::make($image->getRealPath())->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });

        $img->save(storage_path('app/public').$pathImage);

        return config('filesystems.disks.public.path').$pathImage;
    }
}
